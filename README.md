A wide-variety of resources with significantly different capabilities are being offered in a rather crowded cloud computing marketplace. Due to the large number of possible cloud providers and configuration options, applications are typically deployed onto cloud infrastructure ad-hoc with little knowledge of which cloud provider to choose or how to configure a cloud service to maximise an application’s performance and minimise operating costs.

CloudBench was developed to solve this problem and simplify the choice of cloud provider selection. CloudBench was developed in the Big Data Lab at the University of St Andrews, UK and supported by a Royal Society Industry Fellowship and an EPSRC Impact Acceleration Account (IAA) Grant.

CloudBench currently supports two easy to use features:

1. Searching for cloud Virtual Machines (VMs): This feature lists VMs which satisfy a combination of user provided requirements, such as geographic region, operating system, number of computing cores, size of memory, storage and price ($/hr).

2. Selecting cloud VMs: This feature generates performance ranks of cloud VMs based on four weights provided by the user. The weights take a value between 0-5 and indicate how important memory and process, local communication, computation and storage are to the user's application to be deployed the cloud. The research article describing the method that underpins this feature of the prototype is available here.
