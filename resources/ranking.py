
def parse_rank(file_name, is_parallel):
	with open('Parallel-AllWeights.txt') as f: lines = [l.strip() for l in f.readlines() if l.strip() != '' and 'unordered' not in l]

	if is_parallel:
		parallel_flag = '1'
	else:
		parallel_flag = '0'

	ranks = {}

	w = None
	r = None
	for l in lines:
		if l.startswith('[weights]'):
			# parse weight line to get the weights
			w1, w2, w3, w4 = [i.replace('(', ' ').replace(')', '').split()[-1] for i in l.split(' -- ')[1:]]
			w = (w1, w2, w3, w4, parallel_flag)
		elif l.startswith('[ranking]'):
			if w == None or w in ranks: raise Exception(w)
			l = l.replace('[ranking]  ', '')
			vms = [i.split('(')[0] for i in l.split(' -- ')]
			ranks[w] = vms

	return ranks

def write_ranks(ranks):
	with open('rank.out', 'a') as f:
		for ws in ranks:
			s = ''
			for w in ws: s = s + w + '\t'
			for vm in ranks[ws]: s = s + vm + '\t'
			s = s.strip() + '\n'
			f.write(s)