name := "CloudBench"

version := "1.0"

scalaVersion := "2.11.1"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies += "net.ruippeixotog" %% "scala-scraper" % "0.1.1"