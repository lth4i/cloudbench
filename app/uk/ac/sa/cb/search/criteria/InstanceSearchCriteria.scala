package uk.ac.sa.cb.search.criteria

object InstanceSearchCriteria {
  val PROVIDER = "Provider"
  val ZONE = "Zone"
  val OS = "Operating System"
  val CORES = "CPU Cores"
  val MEM = "Memory"
  val STORAGE = "Storage"
  val PRICE = "Price ($/hour)" 
}