package uk.ac.sa.cb.search.sign

import uk.ac.sa.cb.search.criteria.InstanceSearchCriteria

sealed trait Sign {
  def sign: String
  def compare(instanceVal: String, userGivenVal: String): Boolean
}

object Sign {
  def getPossibleSigns(criterion: String): List[Sign] = criterion match {
    case InstanceSearchCriteria.PROVIDER | InstanceSearchCriteria.ZONE | InstanceSearchCriteria.OS => {
      List(StringExactEqualSign)
    }
    case InstanceSearchCriteria.CORES | InstanceSearchCriteria.MEM | InstanceSearchCriteria.STORAGE | InstanceSearchCriteria.PRICE => {
      List(NumberEqualSign, NumberLargerOrEqualSign, NumberLessOrEqualSign, NumberLessSign, NumberLargerSign)
    }
    case _ => throw new IllegalArgumentException("Unknown criterion [%s]".format(criterion))
  }

  def getSign(criterionLabel: String, signString: String): Sign = criterionLabel match {
    case InstanceSearchCriteria.PROVIDER | InstanceSearchCriteria.ZONE | InstanceSearchCriteria.OS => {
      if (signString == StringExactEqualSign.sign) {
        StringExactEqualSign
      } else {
        throw new IllegalArgumentException("Unknown sign [%s] for string comparison".format(signString))
      }
    }
    case InstanceSearchCriteria.CORES | InstanceSearchCriteria.MEM | InstanceSearchCriteria.STORAGE | InstanceSearchCriteria.PRICE => {
      signString match {
        case NumberEqualSign.sign => NumberEqualSign
        case NumberLessOrEqualSign.sign => NumberLessOrEqualSign
        case NumberLessSign.sign => NumberLessSign
        case NumberLargerOrEqualSign.sign => NumberLargerOrEqualSign
        case NumberLargerSign.sign => NumberLargerSign
        case _ => throw new IllegalArgumentException("Unknown sign [%s] for number comparison".format(signString))
      }
    }
    case _ => throw new IllegalArgumentException("Unknown criterion [%s]".format(criterionLabel))
  }
}

object StringExactEqualSign extends Sign {
  val sign = "="
  override def compare(instanceVal: String, userGivenVal: String): Boolean = {
    return instanceVal.contains(userGivenVal)
  }
}

object NumberEqualSign extends Sign {
  val sign = "="
  override def compare(instanceVal: String, userGivenVal: String): Boolean = {
    return BigDecimal(instanceVal) == BigDecimal(userGivenVal)
  }
}

object NumberLargerOrEqualSign extends Sign {
  val sign = ">="
  override def compare(instanceVal: String, userGivenVal: String): Boolean = {
    return BigDecimal(instanceVal) >= BigDecimal(userGivenVal)
  }
}

object NumberLessOrEqualSign extends Sign {
  val sign = "<="
  override def compare(instanceVal: String, userGivenVal: String): Boolean = {
    return BigDecimal(instanceVal) <= BigDecimal(userGivenVal)
  }
}

object NumberLargerSign extends Sign {
  val sign = ">"
  override def compare(instanceVal: String, userGivenVal: String): Boolean = {
    return BigDecimal(instanceVal) > BigDecimal(userGivenVal)
  }
}

object NumberLessSign extends Sign {
  val sign = "<"
  override def compare(instanceVal: String, userGivenVal: String): Boolean = {
    return BigDecimal(instanceVal) < BigDecimal(userGivenVal)
  }
}