package uk.ac.sa.cb.search

import uk.ac.sa.cb.search.sign.Sign
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.search.criteria.InstanceSearchCriteria
import uk.ac.sa.cb.cache.InstancesCache
import uk.ac.sa.cb.label.zone._
import uk.ac.sa.cb.model.search.SearchCriterion

object InstancesSearchService {

  def searchInstances(searchCriteria: List[SearchCriterion]): List[Instance] = {

    val instances = InstancesCache.getInstances

    val satisfiedInstances = searchCriteria.foldLeft(instances)((remainingInstances, currentCriterion) => {
      val criterionLabel = currentCriterion.criterionLabel
      val signString = currentCriterion.signString
      val userGivenVal = currentCriterion.userGivenVal
      remainingInstances.filter(instance => isCriterionSatisfied(criterionLabel, signString, userGivenVal, instance))
    })

    return satisfiedInstances
  }

  def isCriterionSatisfied(criterionLabel: String, signString: String, userGivenVal: String, instance: Instance): Boolean = {
    val sign = Sign.getSign(criterionLabel, signString)
    val instanceVal = getinstanceVal(criterionLabel, instance)
//    println((instanceVal, sign.sign, userGivenVal), sign.compare(instanceVal, userGivenVal))
    return sign.compare(instanceVal, userGivenVal)
  }

  def getinstanceVal(criterionLabel: String, instance: Instance): String = criterionLabel match {
    case InstanceSearchCriteria.PROVIDER => instance.provider.toString
    case InstanceSearchCriteria.ZONE => instance.zone.toString
    case InstanceSearchCriteria.OS => instance.operatingSystem.toString()
    case InstanceSearchCriteria.CORES => instance.cores.toString
    case InstanceSearchCriteria.MEM => instance.mem.toString
    case InstanceSearchCriteria.STORAGE => {
      val storageVol = instance.storageVolume
      if (storageVol == 0) { // instance with 0 storage volume is the one used external storage service
        Int.MaxValue.toString
      } else {
        instance.storageVolume.toString
      }
    }
    case InstanceSearchCriteria.PRICE => instance.price.toString
    case _ => throw new IllegalArgumentException("Unknown criterion [%s]".format(criterionLabel))
  }
}

object InstancesSearchServiceTester extends App {
  val searchCriteria = List(new SearchCriterion(InstanceSearchCriteria.CORES, ">", "4"),
    new SearchCriterion(InstanceSearchCriteria.CORES, "<=", "8"),
    new SearchCriterion(InstanceSearchCriteria.OS, "=", "WINDOWS"),
    new SearchCriterion(InstanceSearchCriteria.ZONE, "=", AMERICA.toString),
    new SearchCriterion(InstanceSearchCriteria.PRICE, "<", "1"),
    new SearchCriterion(InstanceSearchCriteria.STORAGE, ">", "100"))
  val instances = InstancesSearchService.searchInstances(searchCriteria)
  instances.map(i => println(i.getDetailedInstanceInfo))
}