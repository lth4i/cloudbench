package uk.ac.sa.cb.model.search

case class SearchCriteria(val criteria: List[SearchCriterion], val sortLabel: String, val isDescending: Boolean, val pageNumber: Int) {

}