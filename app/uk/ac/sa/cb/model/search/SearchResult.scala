package uk.ac.sa.cb.model.search

import uk.ac.sa.cb.model.instance.Instance

class SearchResult(val returnedInstances: List[Instance], host: String, uri: String,
    currentSortLabel: String, isDescending: Boolean, val pageNumber: Int, val totalPage: Int) {

  private val uriTokens = uri.split("&")
  
  private val url = "http://%s%s".format(host, uri)

  val labels = List("Provider",
    "Zone",
    "Location",
    "Instance Type",
    "Operating System",
    "CPU Cores",
    "Memory (GB)",
    "Storage",
    "Storage Volume (GB)",
    "Price ($/hr)")
    
  val labelUrls = labels.map(label => {
    val labelIsDescending = if (label.toLowerCase() == currentSortLabel.toLowerCase()) {
      // if a label is also a sort label, clicking on it again will change the sort order
      !isDescending
    } else {
      false
    }

    val labelUrl = url
      .replace("sortLabel=%s".format(currentSortLabel.replace(" ", "%20")), "sortLabel=%s".format(label.replace(" ", "%20")))
      .replace("isDescending=%s".format(isDescending), "isDescending=%s".format(labelIsDescending))
      .replace("pageNumber=%s".format(pageNumber), "pageNumber=1")
      
//    val labelUrl = "http://%s%s".format(host, labelUri)
    (label -> labelUrl)
  }).toMap
  
  val firstPage = if (pageNumber == 1) null else url.replace("pageNumber=%s".format(pageNumber), "pageNumber=1")
  val previousPage = if (pageNumber == 1) null else url.replace("pageNumber=%s".format(pageNumber), "pageNumber=%s".format(pageNumber - 1))
  val lastPage = if (pageNumber == totalPage) null else url.replace("pageNumber=%s".format(pageNumber), "pageNumber=%s".format(totalPage))
  val nextPage = if (pageNumber == totalPage) null else url.replace("pageNumber=%s".format(pageNumber), "pageNumber=%s".format(pageNumber + 1))
  
}