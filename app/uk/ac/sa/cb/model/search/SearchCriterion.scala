package uk.ac.sa.cb.model.search

case class SearchCriterion(val criterionLabel: String,
    val signString: String,
    val userGivenVal: String) {

}