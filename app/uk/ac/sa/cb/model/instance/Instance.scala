package uk.ac.sa.cb.model.instance

import uk.ac.sa.cb.label.provider.CloudProvider
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem

abstract class Instance(val provider: CloudProvider,
  val zone: CloudZone, 
  val location: String, 
  val instanceType: String, 
  val operatingSystem: OperatingSystem, 
  val cores: Int, 
  val mem: Double, 
  val storage: String, 
  val price: BigDecimal,
  val pricingUrl: String) extends Equals {
  
  def storageVolume:Int = storage.toInt
  
  override def toString: String = {
    return "Instance[Provider:%s][Zone:%s][Location:%s][Type:%s][OS:%s][Price:%s]".format(provider, zone, location, instanceType, operatingSystem, price)
  }
  
  def getDetailedInstanceInfo:String = {
    return "%s[Cores:%s][Mem:%s][Storage:%s][StorageVol:%s]"
    .format(this.toString, cores, mem, storage, storageVolume)
  }

  def canEqual(other: Any) = {
    other.isInstanceOf[uk.ac.sa.cb.model.instance.Instance]
  }

  override def equals(other: Any) = {
    other match {
      case that: uk.ac.sa.cb.model.instance.Instance => that.canEqual(Instance.this) && provider == that.provider && zone == that.zone && location == that.location && instanceType == that.instanceType && operatingSystem == that.operatingSystem
      case _ => false
    }
  }

  override def hashCode() = {
    val prime = 41
    prime * (prime * (prime * (prime * (prime + provider.hashCode) + zone.hashCode) + location.hashCode) + instanceType.hashCode) + operatingSystem.hashCode
  }
}