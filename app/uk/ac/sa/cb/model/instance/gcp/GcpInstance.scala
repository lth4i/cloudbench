package uk.ac.sa.cb.model.instance.gcp

import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem
import uk.ac.sa.cb.label.provider.GCP
import uk.ac.sa.cb.label.os.LINUX

/**
 * Google Cloud Platform instance
 */
class GcpInstance(zone: CloudZone,
  location: String,
  instanceType: String,
  operatingSystem: OperatingSystem,
  cores: Int,
  mem: Double,
  storage: String,
  price: BigDecimal,
  val gCEU: String) extends Instance(GCP, zone, location, instanceType, operatingSystem, cores, mem, storage, price, "https://cloud.google.com/pricing/") {

  def this(zone: CloudZone,
    location: String,
    instanceType: String,
    cores: Int,
    mem: Double,
    storage: String,
    price: BigDecimal,
    gCEU: String) = {
    this(zone, location, instanceType, LINUX, cores, mem, storage, price, gCEU)
  }

  override val getDetailedInstanceInfo: String = {
    "%s[%s]".format(super.getDetailedInstanceInfo, gCEU)
  }
  
}