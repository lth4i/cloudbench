package uk.ac.sa.cb.model.instance.azure

import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.label.provider.AZURE

class AzureInstance(zone: CloudZone,
  location: String,
  instanceType: String,
  operatingSystem: OperatingSystem,
  cores: Int,
  mem: Double,
  storage: String,
  price: BigDecimal) extends Instance(AZURE, zone, location, instanceType, operatingSystem, cores, mem, storage, price, "http://azure.microsoft.com/en-gb/pricing/details/virtual-machines/") {

  override val storageVolume: Int = storage.replace("GB", "").replace(",", "").trim.toInt
  
}