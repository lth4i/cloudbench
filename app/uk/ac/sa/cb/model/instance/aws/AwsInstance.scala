package uk.ac.sa.cb.model.instance.aws

import uk.ac.sa.cb.label.provider.AWS
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem

class AwsInstance(zone: CloudZone,
  location: String,
  instanceType: String,
  operatingSystem: OperatingSystem,
  cores: Int,
  mem: Double,
  storage: String,
  price: BigDecimal,
  val ecu: String,
  val networkPerformance: String,
  val physicalProcessor: String,
  val clockSpeed: String,
  val isIntelAVXSupported: Boolean,
  val isIntelAVX2Supported: Boolean,
  val isIntelTurboSupported: Boolean,
  val isEbsOptSupported: Boolean,
  val isEnhancedNetworkingSupported: Boolean) extends Instance(AWS, zone, location, instanceType, operatingSystem, cores, mem, storage.replace("ebsonly","EBS Only"), price, "http://aws.amazon.com/ec2/pricing/") {

  def this(zone: CloudZone,
  location: String,
    instanceType: String,
    operatingSystem: OperatingSystem,
    cores: Int,
    mem: Double,
    storage: String,
    price: BigDecimal,
    ecu: String) = {
    this(zone, location, instanceType, operatingSystem, cores, mem, storage, price, ecu, null, null, null, false, false, false, false, false)
  }

  def this(basicInstance: AwsInstance,
    detailMatrix: AwsInstanceDetailMatrix) = {
    this(basicInstance.zone,
      basicInstance.location,
      basicInstance.instanceType,
      basicInstance.operatingSystem,
      basicInstance.cores, basicInstance.mem,
      basicInstance.storage,
      basicInstance.price,
      basicInstance.ecu,
      detailMatrix.networkPerformance,
      detailMatrix.physicalProcessor,
      detailMatrix.clockSpeed,
      detailMatrix.isIntelAVXSupported,
      detailMatrix.isIntelAVX2Supported,
      detailMatrix.isIntelTurboSupported,
      detailMatrix.isEbsOptSupported,
      detailMatrix.isEnhancedNetworkingSupported)
    if (detailMatrix.instanceType != instanceType) {
      throw new IllegalArgumentException("Cannot assign performance matrix of instance type [%s] to instance type [%s]"
        .format(detailMatrix.instanceType, this.instanceType))
    }
  }

  override val storageVolume: Int = {
    if ("ebsonly" == storage || "EBS Only" == storage) {
      0
    } else {
      val tokens = storage.replace("SSD", "").split("x").map(_.trim).map(_.toInt)
      if (tokens.size == 1) {
        tokens(0).toInt
      } else if (tokens.size == 2) {
        val numOfDrive = tokens(0)
        val volumeOfEachDrive = tokens(1)
        numOfDrive * volumeOfEachDrive
      } else {
        throw new IllegalArgumentException("Cannot parse aws storage [%s] to storage volume".format(storage))
      }
    }
  }

  override val getDetailedInstanceInfo: String = {
    "%s[%s][%s][%s][%s][%s][%s][%s][%s]"
      .format(super.getDetailedInstanceInfo, networkPerformance,
        physicalProcessor, clockSpeed, isIntelAVXSupported, isIntelAVX2Supported, isIntelAVX2Supported, isEbsOptSupported, isEnhancedNetworkingSupported)
  }

}