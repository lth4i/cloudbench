package uk.ac.sa.cb.model.instance.aws

class AwsInstanceDetailMatrix(val instanceType: String,
  val networkPerformance: String,
  val physicalProcessor: String,
  val clockSpeed: String,
  val isIntelAVXSupported: Boolean,
  val isIntelAVX2Supported: Boolean,
  val isIntelTurboSupported: Boolean,
  val isEbsOptSupported: Boolean,
  val isEnhancedNetworkingSupported: Boolean) {

}