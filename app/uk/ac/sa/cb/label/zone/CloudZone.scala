package uk.ac.sa.cb.label.zone

import uk.ac.sa.cb.label.Label

sealed trait CloudZone extends Label with Ordered[CloudZone] {
  def cloudZone: String
  override def toString: String = cloudZone
  
  def compare(that: CloudZone): Int = {
    this.cloudZone.compare(that.cloudZone)
  }
}

object CloudZone {
  
  val allZones = List(AMERICA, EUROPE, ASIA, AUSTRALIA).map(_.toString)
  
  def getZoneFromString(s: String): CloudZone = s match {
    case AMERICA.cloudZone => AMERICA
    case EUROPE.cloudZone => EUROPE
    case ASIA.cloudZone => ASIA
    case AUSTRALIA.cloudZone => AUSTRALIA
    case _ => throw new IllegalArgumentException("Unknown Cloud Zone [%s]".format(s))
  }
}

case object AMERICA extends CloudZone { val cloudZone = "AMERICA" }
case object EUROPE extends CloudZone { val cloudZone = "EUROPE" }
case object ASIA extends CloudZone { val cloudZone = "ASIA" }
case object AUSTRALIA extends CloudZone { val cloudZone = "AUSTRALIA" }