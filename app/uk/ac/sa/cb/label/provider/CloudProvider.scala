package uk.ac.sa.cb.label.provider

import uk.ac.sa.cb.label.Label

sealed trait CloudProvider extends Label with Ordered[CloudProvider] {
  def providerName: String
  
  def compare(that: CloudProvider): Int = {
    this.providerName.compare(that.providerName)
  }
  
  override def toString: String = return providerName
}

object CloudProvider {
  
  val allProviders = List(AWS, AZURE, GCP)
  
  def getProviderFromString(s:String): CloudProvider = {
    for (provider <- allProviders) {
      if (provider.providerName == s) {
        return provider
      }
    }
    throw new IllegalArgumentException("Unknown cloud provider [%s]".format(s))
  }
  
  
}

case object AWS extends CloudProvider { val providerName = "Amazon Web Service" }
case object AZURE extends CloudProvider { val providerName = "Microsoft Azure" }
case object GCP extends CloudProvider { val providerName = "Google Cloud Platform"}