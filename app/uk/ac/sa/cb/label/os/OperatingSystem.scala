package uk.ac.sa.cb.label.os

import uk.ac.sa.cb.label.Label

sealed trait OperatingSystem extends Label with Ordered[OperatingSystem] {
  def osName: String
  
  def compare(that: OperatingSystem): Int = {
    this.osName.compare(that.osName)
  }
}

object OperatingSystem {
  
  val allOperatingSystems = List(LINUX, SUSE_LINUX_ENTERPRISE_SERVER, RED_HAT_ENTERPRISE_LINUX, WINDOWS,
      WINDOWS_WITH_SQL_STANDARD, WINDOWS_WITH_SQL_WEB, WINDOWS_WITH_SQL_ENTERPRISE,
      WINDOWS_WITH_BIZTALK_STANDARD, WINDOWS_WITH_BIZTALK_ENTERPRISE, WINDOWS_WITH_ORACLE_JAVA_SE, WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_SE,
      WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_EE, WINDOWS_WITH_ORACLE_DATABASE_SE, WINDOWS_WITH_ORACLE_DATABASE_EE)
  
  val operatingSystemsForSearch = List(LINUX, WINDOWS)
      
  def getOperatingSystemFromString(s: String): OperatingSystem = {
    for (os <- allOperatingSystems) {
      if (os.osName == s) {
        return os
      }
    }
    throw new IllegalArgumentException("Unknown Operating System String [%s]".format(s))
  }
}

case object LINUX extends OperatingSystem { val osName = "LINUX" }
case object SUSE_LINUX_ENTERPRISE_SERVER extends OperatingSystem { val osName = "SUSE_LINUX_ENTERPRISE_SERVER" }
case object RED_HAT_ENTERPRISE_LINUX extends OperatingSystem { val osName = "RED_HAT_ENTERPRISE_LINUX" }

case object WINDOWS extends OperatingSystem { val osName = "WINDOWS" }
case object WINDOWS_WITH_SQL_STANDARD extends OperatingSystem { val osName = "WINDOWS_WITH_SQL_STANDARD" }
case object WINDOWS_WITH_SQL_WEB extends OperatingSystem { val osName = "WINDOWS_WITH_SQL_WEB" }
case object WINDOWS_WITH_SQL_ENTERPRISE extends OperatingSystem { val osName = "WINDOWS_WITH_SQL_ENTERPRISE" }

case object WINDOWS_WITH_BIZTALK_STANDARD extends OperatingSystem { val osName = "WINDOWS_WITH_BIZTALK_STANDARD" }
case object WINDOWS_WITH_BIZTALK_ENTERPRISE extends OperatingSystem { val osName = "WINDOWS_WITH_BIZTALK_ENTERPRISE" }

case object WINDOWS_WITH_ORACLE_JAVA_SE extends OperatingSystem { val osName = "WINDOWS_WITH_ORACLE_JAVA_SE" }
case object WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_SE extends OperatingSystem { val osName = "WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_SE" }
case object WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_EE extends OperatingSystem { val osName = "WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_EE" }
case object WINDOWS_WITH_ORACLE_DATABASE_SE extends OperatingSystem { val osName = "WINDOWS_WITH_ORACLE_DATABASE_SE" }
case object WINDOWS_WITH_ORACLE_DATABASE_EE extends OperatingSystem { val osName = "WINDOWS_WITH_ORACLE_DATABASE_EE" }
