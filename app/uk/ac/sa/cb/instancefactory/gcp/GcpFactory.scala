package uk.ac.sa.cb.instancefactory.gcp

import uk.ac.sa.cb.utils.PageSourceRetrievingService
import org.jsoup.Jsoup
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import org.jsoup.nodes.Element
import uk.ac.sa.cb.label.zone._
import uk.ac.sa.cb.label.os._
import uk.ac.sa.cb.model.instance.gcp.GcpInstance
import uk.ac.sa.cb.model.instance.gcp.GcpInstance
import uk.ac.sa.cb.instancefactory.AbstractInstanceFactory
import uk.ac.sa.cb.model.instance.Instance

object GcpFactory extends AbstractInstanceFactory {

  val INSTANCE_TYPE = 0
  val CORES = 1
  val MEM = 2
  val GCEU = 3
  val FULL_PRICE = 6

  val SHARED_CORE_INSTANCES = List("f1-micro", "g1-small")

  override def getInstances(): List[Instance] = {
    val pageSourceText = PageSourceRetrievingService.getPageSource("https://cloud.google.com/pricing/", "resources/gcp_source")
    val pageSource = Jsoup.parse(pageSourceText)
    val articleElements = pageSource.getElementsByTag("article")

    val instancesWithBasicLinux = articleElements.map(parseArticleElement(_)).flatten.toList

    val pricingTableContents = pageSource.getElementsByClass("pricing-table-content")
    val premiumOsTableContents = pricingTableContents(1)
    val instancesWithPremiumOs = parsePremiumOsTable(premiumOsTableContents, instancesWithBasicLinux)

    val allInstances = instancesWithBasicLinux ++ instancesWithPremiumOs

    return allInstances

  }

  override def converZoneStringToCloudZone(zoneString: String): CloudZone = {
    val cloudZone = zoneString match {
      case "us" => AMERICA
      case "eu" => EUROPE
      case _ => throw new IllegalArgumentException("Unknown Google Cloud Engine region [%s]".format(zoneString))
    }
    return cloudZone
  }
  
  def parseArticleElement(articleElem: Element): List[GcpInstance] = {
    val header = articleElem.getElementsByTag("header")(0)
    val locationString = header.attr("id")

    val cloudZone = converZoneStringToCloudZone(locationString)
    
    val os = LINUX // default operating system for google cloud platform

    val tbodies = articleElem.getElementsByTag("tbody")
    val instances = tbodies.map(tbody => {
      val trs = tbody.getElementsByTag("tr")

      trs.tail.map(tr => { // ignore first row as it is the header row
        val tds = tr.getElementsByTag("td")
        val instanceType = tds(INSTANCE_TYPE).text
        val cores = tds(CORES).text.toInt
        val mem = tds(MEM).text.toLowerCase.replace("gb", "").toDouble
        val gCEU = tds(GCEU).text
        val price = BigDecimal(tds(FULL_PRICE).text.replace("$", ""))

        val storage = if (SHARED_CORE_INSTANCES.contains(instanceType)) {
          (3 * 2014).toString
        } else {
          (5 * 2014).toString
        }

        val instance = new GcpInstance(cloudZone, locationString, instanceType,
          os, cores, mem, storage, price, gCEU)
        instance
      })
    }).flatten.toList

    return instances
  }

  def parsePremiumOsTable(premimuOsTable: Element, instancesWithBasicLinux: List[GcpInstance]): List[GcpInstance] = {
    val trs = premimuOsTable.getElementsByTag("tr")
    val instancesWithPremiumOs = trs.map(tr => {
      val tds = tr.getElementsByTag("td")
      val osString = tds(0).text
      val os = osString match {
        case "Windows server images" => WINDOWS
        case "SUSE images" => SUSE_LINUX_ENTERPRISE_SERVER
        case "Red Hat Enterprise Linux (RHEL) images" => RED_HAT_ENTERPRISE_LINUX
        case _ => throw new IllegalArgumentException("Unknow Google Cloud Platform Premium Operating System: [%s]".format(osString))
      }
      val prices = tds(1).text.split(" ")
        .filter(_.contains("$")) // get price only
        .map(_.replace("$", "")) // remove '$' symbol
        .map(_.replace("/hour", "")).toList // remove '/hour' symbol

      val firstCase = BigDecimal(prices(0))
      val secondCase = BigDecimal(prices(1))

      instancesWithBasicLinux.map(instance => {
        val newPrice = os match {
          case RED_HAT_ENTERPRISE_LINUX => {
            if (instance.cores < 8) {
              instance.price + firstCase
            } else {
              instance.price + secondCase
            }
          }
          case SUSE_LINUX_ENTERPRISE_SERVER => {
            if (SHARED_CORE_INSTANCES.contains(instance.instanceType)) {
              instance.price + firstCase
            } else {
              instance.price + secondCase
            }
          }
          case WINDOWS => {
            if (SHARED_CORE_INSTANCES.contains(instance.instanceType)) {
              instance.price + firstCase
            } else {
              instance.price + secondCase * instance.cores
            }
          }
          case _ => throw new IllegalArgumentException("Google Cloud Platform does not have [%s] premium OS".format(os))
        }
        new GcpInstance(instance.zone, instance.location, instance.instanceType,
          os, instance.cores, instance.mem, instance.storage, newPrice, instance.gCEU)
      })
    }).flatten.toList

    return instancesWithPremiumOs
  }
}