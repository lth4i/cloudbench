package uk.ac.sa.cb.instancefactory.azure

import org.jsoup.nodes.Element
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._

object AzureInstanceBasicDetailParser {

  val NUM_TD_TAGS_IN_TR_TAG = 5
  val INSTANCE_NAME = 0
  val CPU_CORES = 1
  val MEM = 2
  val STORAGE = 3
  val PRICE = 4

  def parseTable(table: Element): Map[String, (String, String, String, Map[String, String])] = {
    val trs = table.getElementsByTag("tr")

    val basicDetails = trs.foldLeft(Map[String, (String, String, String, Map[String, String])]())((result, tr) => {
      try {
      val tds = tr.getElementsByTag("td")
      val instanceName = tds(INSTANCE_NAME).text
      val cpuCores = tds(CPU_CORES).text
      val mem = tds(MEM).text
      val storage = tds(STORAGE).text
      val locationToPrice = parseHtmlPrice(tds(PRICE))
      
      result.updated(instanceName, (cpuCores, mem, storage, locationToPrice))
      } catch {
        case _:IndexOutOfBoundsException => result
      }
    })
    
    return basicDetails
  }

  private def parseHtmlPrice(htmlPrice: Element): Map[String, String] = {
    val htmlSpans = htmlPrice.getElementsByClass("wa-conditionalDisplay")
    val locationToPrice = htmlSpans.foldLeft(Map[String, String]())((result, span) => {
      val location = span.attr("data-condition-region")
      val priceString = span.text
      if (priceString.endsWith("hr")) { // get price per hour only
        val price = priceString.replace("$", "").replace("/", "").replace("hr", "")
        result.updated(location, price)
      } else {
        result
      }
    })

    return locationToPrice
  }
}