package uk.ac.sa.cb.instancefactory.azure

import org.jsoup.nodes.Element
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import uk.ac.sa.cb.model.instance.azure.AzureInstance
import uk.ac.sa.cb.label.os.LINUX
import uk.ac.sa.cb.label.os.SUSE_LINUX_ENTERPRISE_SERVER
import uk.ac.sa.cb.model.instance.azure.AzureInstance

object AzureSlesParser {
  
  def createSlesInstances(table:Element, instances:List[AzureInstance]): List[AzureInstance] = {
    val prices = parseSlesTable(table)
    val slesInstances = instances.map(instance => {
      if (instance.operatingSystem == LINUX && prices.contains(instance.instanceType)) {
        val newPrice = BigDecimal(prices(instance.instanceType)) + instance.price
        new AzureInstance(instance.zone,
            instance.location,
            instance.instanceType,
            SUSE_LINUX_ENTERPRISE_SERVER,
            instance.cores,
            instance.mem,
            instance.storage,
            newPrice)
      } else {
        null
      }
    }).filterNot(_ == null).toList
    return slesInstances
  }
  
  def parseSlesTable(table: Element): Map[String, String] = {
    val trs = table.getElementsByTag("tbody")(0).getElementsByTag("tr")
    val instanceTypeToAdditionalPrice = trs.map(tr => {
      val tds = tr.getElementsByTag("td")
      val instanceName = tds(0).text
      val price = tds(3).text.replace("$", "").replace("/", "").replace("hr", "")
      (instanceName -> price)
    }).toMap
    return instanceTypeToAdditionalPrice
  }
}