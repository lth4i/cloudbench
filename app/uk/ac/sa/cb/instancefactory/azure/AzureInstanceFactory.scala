package uk.ac.sa.cb.instancefactory.azure

import scala.io.Source
import org.jsoup.Jsoup
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import uk.ac.sa.cb.instancefactory.AbstractInstanceFactory
import uk.ac.sa.cb.label.os.OperatingSystem
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.utils.FileWriterService
import uk.ac.sa.cb.utils.PageSourceRetrievingService
import uk.ac.sa.cb.label.zone._

object AzureInstanceFactory extends AbstractInstanceFactory {
  override def getInstances(): List[Instance] = {
    
    val instances = AzureWebCrawlingService.createInstances
    

    return instances
  }

  override def convertOsStringToOs(osString: String): OperatingSystem = null

  override def converZoneStringToCloudZone(zoneString: String): CloudZone = zoneString match {
    case "us-east-2" | "us-west" | "us-central" |
      "brazil-south" | "us-south-central" | "us-north-central" |
      "usgov-iowa" | "us-east" | "usgov-virginia" => return AMERICA
    case "asia-pacific-east" | "japan-west" | "asia-pacific-southeast" | "japan-east" => return ASIA
    case "australia-east" | "australia-southeast" => return AUSTRALIA
    case "europe-north" | "europe-west" => return EUROPE
    case _ => throw new IllegalArgumentException("Unknown Azure cloud location [%s]".format(zoneString))
  }
}