package uk.ac.sa.cb.instancefactory.azure

import org.jsoup.nodes.Element
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import org.jsoup.select.Elements
import uk.ac.sa.cb.model.instance.azure.AzureInstance
import uk.ac.sa.cb.label.os._

sealed trait AdditionalPackage {
  def tableNumbers: List[Int]
}

object AzureAdditionalPackageParser {

  def createInstancesWithAdditionalPackage(table: Element, instances: List[AzureInstance]): List[AzureInstance] = {
    val instanceTypeToAdditionalPackageToPrice = parseOsTable(table)

    val newInstances = instances.map(instance => {
      if (instance.operatingSystem == WINDOWS && instanceTypeToAdditionalPackageToPrice.contains(instance.instanceType)) {
        val additionalPackageToPrice = instanceTypeToAdditionalPackageToPrice(instance.instanceType)

        additionalPackageToPrice.map(additionalPackageAndPrice => {
          val (pkg, price) = additionalPackageAndPrice
          val os = getOs(pkg)
          val newPrice = BigDecimal(price) + instance.price
          new AzureInstance(instance.zone,
            instance.location,
            instance.instanceType,
            os,
            instance.cores,
            instance.mem,
            instance.storage,
            newPrice)
        })
      } else {
        null
      }
    }).filterNot(_ == null).flatten.toList
    
    return newInstances
  }

  def getOs(osString: String): OperatingSystem = osString.toUpperCase() match {
    case "SQL WEB" => WINDOWS_WITH_SQL_WEB
    case "SQL STANDARD" => WINDOWS_WITH_SQL_STANDARD
    case "SQL ENTERPRISE" => WINDOWS_WITH_SQL_ENTERPRISE
    case "BIZTALK STANDARD" => WINDOWS_WITH_BIZTALK_STANDARD
    case "BIZTALK ENTERPRISE" => WINDOWS_WITH_BIZTALK_ENTERPRISE
    case "JAVA (SE)" => WINDOWS_WITH_ORACLE_JAVA_SE
    case "ORACLE WEBLOGIC SERVER (SE)" => WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_SE
    case "ORACLE WEBLOGIC SERVER (EE)" => WINDOWS_WITH_ORACLE_WEB_LOGIC_SERVER_EE
    case "ORACLE DATABASE (SE)" => WINDOWS_WITH_ORACLE_DATABASE_SE
    case "ORACLE DATABASE (EE)" => WINDOWS_WITH_ORACLE_DATABASE_EE
    case _ => throw new IllegalArgumentException("Unknown Azure type [%s]".format(osString))
  }

  def parseOsTable(table: Element): Map[String, Map[String, String]] = {
    val indexToAddtionalPackageName = getPackageNames(table)

    val trs = table.getElementsByTag("tr")
    val instanceToPrices = trs.foldLeft(Map[String, Map[String, String]]())((result, tr) => {
      val tds = tr.getElementsByTag("td")
      if (tds.size > 0) {
        val htmlInstances = tds(0)
        val additionalPackageToPrice = indexToAddtionalPackageName.foldLeft(Map[String, String]())((result, pair) => {
          val (index, name) = pair
          val price = tds(index).text.replace("$", "").replace("/", "").replace("hr", "").split(" ")(0)
          result.updated(name, price)
        })

        val instances = getBasicAndStandardInstances(htmlInstances.text)

        instances.foldLeft(result)((internalResult, instance) => {
          internalResult.updated(instance, additionalPackageToPrice)
        })
      } else {
        result
      }
    })

    return instanceToPrices
  }

  private def getPackageNames(element: Element): Map[Int, String] = {
    val ths = element.getElementsByTag("th")
    if (ths.size > 2) {
      val indexToAdditionalPackageName = (2 until ths.size).map(index => {
        var name = ths(index).toString
        ths(index).children().toList.map(c => {
          name = name.replace(c.toString, "")
        })
        name = name.replace("<th>", "").replace("</th>", "")
        index -> name
      }).toMap
      return indexToAdditionalPackageName
    } else {
      return Map[Int, String]()
    }
  }

  private def getBasicAndStandardInstances(string: String): List[String] = {
    var basic = ""
    var standard = ""
    if (string.contains("Basic or Standard")) {
      val tokens = string.split("Basic or Standard")
      basic = tokens(0).trim
      if (tokens.size == 1) {
        standard = ""
      } else {
        standard = tokens(1).replace("Standard", "").trim
      }
    } else {
      basic = ""
      standard = string.replace("Standard", "").trim
    }

    val basicInstances = basic.split(",").map(_.trim()).toList.filterNot(_.trim == "")
    val standardInstances = (basicInstances ++ standard.split(",").map(_.trim()).toList).filterNot(_.trim == "")
    return basicInstances.map("%s.basic".format(_)) ++ standardInstances
  }

  def getAdditionalPackages(tables: Elements): Map[String, Map[String, String]] = {
    val instanceToAdditionalPackageToPrice = (13 to 21).foldLeft(Map[String, Map[String, String]]())((result, num) => {
      val tempMap = parseOsTable(tables(num))
      tempMap.foldLeft(result)((newResult, item) => {
        val (instance, additionalPackageToPrice) = item
        if (result.contains(instance)) {
          newResult.updated(instance, (result(instance).toList ++ additionalPackageToPrice.toList).toMap)
        } else {
          newResult.updated(instance, additionalPackageToPrice)
        }
      })
    })
    return instanceToAdditionalPackageToPrice
  }

}