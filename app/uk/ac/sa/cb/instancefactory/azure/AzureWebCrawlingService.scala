package uk.ac.sa.cb.instancefactory.azure

import org.jsoup.Jsoup
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import org.jsoup.select.Elements
import org.jsoup.nodes.Element
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.label.provider.AZURE
import uk.ac.sa.cb.label.os._
import uk.ac.sa.cb.model.instance.azure.AzureInstance
import uk.ac.sa.cb.model.instance.azure.AzureInstance
import uk.ac.sa.cb.utils.PageSourceRetrievingService

object AzureWebCrawlingService {

  val WINDOWS_BASIC_TIER = 0
  val WINDOWS_STANDARD_TIER = 1
  val WINDOWS_COMPUTE_OPTIMISED_TIER = 2
  val WINDOWS_PERFORMANCE_OPTIMISED_TIER = 3
  val WINDOWS_NETWORK_OPTIMISED_TIER = 4
  val WINDOWS_COMPUTE_INTENSIVE_TIER = 5

  val LINUX_BASIC_TIER = 6
  val LINUX_STANDARD_TIER = 7
  val LINUX_COMPUTE_OPTIMISED_TIER = 8
  val LINUX_PERFORMANCE_OPTIMISED_TIER = 9
  val LINUX_NETWORK_OPTIMISED_TIER = 10
  val LINUX_COMPUTE_INTENSIVE_TIER = 11

  val LINUX_SLE = 12

  val SQL_WEB = 2
  val SQL_STANDARD = 3
  val SQL_ENTERPRISE = 4

  def createInstances: List[AzureInstance] = {
    val pageSource = PageSourceRetrievingService.getPageSource("http://azure.microsoft.com/en-gb/pricing/details/virtual-machines/", "resources/azure_source")
    val tables = getHtmlTables(pageSource)
    
    // create basic instances
    val basicWinInstances = createBasicInstances(tables, 0, 0, WINDOWS, true)
    val basicLinuxInstances = createBasicInstances(tables, 6, 6, LINUX, true)
    
    // create other (non-basic) instances
    val winInstances = basicWinInstances ++ createBasicInstances(tables, 1, 5, WINDOWS, false)
    val linuxInstances = basicLinuxInstances ++ createBasicInstances(tables, 7, 11, LINUX, false)
    
//    (winInstances ++ linuxInstances).map(i => println(i.getDetailedInstanceInfo))
    // create SLES instances
    val slesInstances = AzureSlesParser.createSlesInstances(tables(LINUX_SLE), linuxInstances)
//    slesInstances.map(i => println(i.getDetailedInstanceInfo))
    
    // instances with additional package
    val instancesWithAdditionalPackage = (13 to 21).map(num => {
      AzureAdditionalPackageParser.createInstancesWithAdditionalPackage(tables(num), winInstances)     
    }).flatten
//    instancesWithAdditionalPackage.map(i => println(i.getDetailedInstanceInfo))
//    println(instancesWithAdditionalPackage.size)
    
    val allAzureInstances = linuxInstances ++ winInstances ++ slesInstances ++ instancesWithAdditionalPackage
    
    println(allAzureInstances.size)
    
    return allAzureInstances
  }

  def createBasicInstances(tables: Elements,
    startIndex: Int, endIndex: Int, os: OperatingSystem, isBasic: Boolean): List[AzureInstance] = {
    val instances = (startIndex to endIndex).map(num => {
      val table = tables(num)
      val basicDetails = AzureInstanceBasicDetailParser.parseTable(table)

      val instances = basicDetails.map(item => {
        val (instanceName, (cpuCores, mem, storage, locationToPrice)) = item
        locationToPrice.map(locationAndPrice => {
          val (location, price) = locationAndPrice
          new AzureInstance(AzureInstanceFactory.converZoneStringToCloudZone(location),
            location,
            if(isBasic) "%s.basic".format(instanceName) else instanceName,
            os,
            cpuCores.toInt,
            mem.replace("GB", "").toDouble,
            storage,
            BigDecimal(price))
        })
      }).flatten
      instances
    }).flatten.toList
    return instances
  }

  def getHtmlTables(pageSource: String): Elements = {
    val doc = Jsoup.parse(pageSource)
    val eles = doc.getAllElements()

    val waSections = doc >> elements(".wa-section")
    val waSectionWithInstanceDetails = waSections(2)

    val tables = waSectionWithInstanceDetails.getElementsByTag("table")

    return tables
  }

  
//  createInstances
  //  val tables = getHtmlTables
  //  val tmp = AzureAdditionalPackageParser.getAdditionalPackages(tables)
  //  tmp.map(item => {
  //    val (instance, tmp1) = item
  ////    println(instance, tmp1.size)
  //    tmp1.map(item1 => {
  //      val (pkd, price) = item1
  //      println(instance, pkd, price)
  //    })
  //  })
  //  AwsAdditionalPackageParser.parseOsTable(htmlTables(13))
}