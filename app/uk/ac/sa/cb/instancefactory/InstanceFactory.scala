package uk.ac.sa.cb.instancefactory

import uk.ac.sa.cb.instancefactory.aws.AwsInstanceFactory
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem
import uk.ac.sa.cb.label.provider._
import uk.ac.sa.cb.label.provider.CloudProvider
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.instancefactory.azure.AzureInstanceFactory
import uk.ac.sa.cb.instancefactory.gcp.GcpFactory

object InstanceFactory extends AbstractInstanceFactory {

  val providerToFactory = Map[CloudProvider, AbstractInstanceFactory](AWS -> AwsInstanceFactory,
      AZURE -> AzureInstanceFactory, GCP -> GcpFactory)

  override def getInstances(): List[Instance] = {
    val allCloudInstances = providerToFactory.map(keyAndValue => {
      val (provider, factory) = keyAndValue
      factory.getInstances
    }).flatten.toList
    return allCloudInstances
  }
}