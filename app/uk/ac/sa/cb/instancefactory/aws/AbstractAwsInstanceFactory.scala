package uk.ac.sa.cb.instancefactory.aws

import uk.ac.sa.cb.instancefactory.AbstractInstanceFactory
import uk.ac.sa.cb.label.zone._
import uk.ac.sa.cb.label.os._

trait AbstractAwsInstanceFactory extends AbstractInstanceFactory {

  override def converZoneStringToCloudZone(zoneString: String): CloudZone = zoneString match {
    case "us-east-1" | "us-west-2" | "us-west-1" | "sa-east-1" | "us-gov-west-1" => return AMERICA
    case "eu-west-1" | "eu-central-1" => return EUROPE
    case "ap-southeast-1" | "ap-northeast-1" => return ASIA
    case "ap-southeast-2" => return AUSTRALIA
    case _ => throw new IllegalArgumentException("Unknow AWS zone string [%s]".format(zoneString))
  }
  
  override def convertOsStringToOs(osString: String): OperatingSystem = osString match {
    case "linux" => return LINUX
    case "mswin" => return WINDOWS
    case "rhel" => return RED_HAT_ENTERPRISE_LINUX
    case "sles" => return SUSE_LINUX_ENTERPRISE_SERVER
    case "mswinSQL" => return WINDOWS_WITH_SQL_STANDARD
    case "mswinSQLWeb" => return WINDOWS_WITH_SQL_WEB
    case _ => throw new IllegalArgumentException("Unknow AWS operating system string [%s]".format(osString))
  }
}