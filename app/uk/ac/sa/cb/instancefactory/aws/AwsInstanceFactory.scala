package uk.ac.sa.cb.instancefactory.aws

import uk.ac.sa.cb.instancefactory.aws.basic.BasicAwsInstanceFactory
import uk.ac.sa.cb.model.instance.aws.AwsInstance
import uk.ac.sa.cb.model.instance.aws.AwsInstanceDetailMatrix
import uk.ac.sa.cb.instancefactory.aws.detailmatrix.AwsInstanceDetailMetrixGetter

object AwsInstanceFactory extends AbstractAwsInstanceFactory {

  override def getInstances: List[AwsInstance] = {
    val basicAwsInstances = BasicAwsInstanceFactory.getInstances;
    val detailMatrices = AwsInstanceDetailMetrixGetter.getDetailMatrices
    val awsInstances = addDetailMatrixToInstance(detailMatrices, basicAwsInstances)
    return awsInstances
  }
  
  private def addDetailMatrixToInstance(detailMatrices: Map[String, AwsInstanceDetailMatrix], basicAwsInstances:List[AwsInstance]): List[AwsInstance] = {
    val awsInstances = basicAwsInstances.map(basicInstance => {
      val detailMatrix = detailMatrices(basicInstance.instanceType)
      new AwsInstance(basicInstance, detailMatrix)
    })
    return awsInstances
  } 
}