package uk.ac.sa.cb.instancefactory.aws.basic

import scala.BigDecimal

import play.api.libs.json.JsArray
import play.api.libs.json.JsValue
import uk.ac.sa.cb.model.instance.aws.AwsInstance

object AwsInstanceJsonParser {

  def parseJsonsToBasicInstances(jsons: List[JsValue]): List[AwsInstance] = {
    return jsons.map(json => {
      val regionsJsResult = (json \ "config" \ "regions").validate[JsArray] // get regions
      if (regionsJsResult.isSuccess) {
        parseMultipleRegionsJs(regionsJsResult.get.value)
      } else {
        List[AwsInstance]()
      }
    }).flatten
  }

  private def parseMultipleRegionsJs(regions: Seq[JsValue]): List[AwsInstance] = {
    val regionsMap = regions.foldLeft(List[AwsInstance]())((regionResult, regionBranch) => {
      val regionJsResult = (regionBranch \ "region").validate[String] // get region name
      val instanceTypesJsResult = (regionBranch \ "instanceTypes").validate[JsArray] // get instance types branch of region
      if (regionJsResult.isSuccess && instanceTypesJsResult.isSuccess) {
        val region = regionJsResult.get
        val instanceTypes = instanceTypesJsResult.get.value

        val instanceTypesMap = instanceTypes.foldLeft(List[AwsInstance]())((instanceTypeResult, typeBranch) => {
          instanceTypeResult ++ parseTypeBranchJs(typeBranch, region)
        })
        regionResult ++ instanceTypesMap
      } else {
        regionResult
      }
    })
    //      regionsMap.map(println(_))
    return regionsMap
  }

  private def parseTypeBranchJs(typeBranch: JsValue, region: String): List[AwsInstance] = {
    //    val typeJsResult = (typeBranch \ "type").validate[String] // get instance type
    val sizesJsResult = (typeBranch \ "sizes").validate[JsArray] // get instance sizes of type
    if (sizesJsResult.isSuccess) {
      //      val instanceType = typeJsResult.get
      val sizes = sizesJsResult.get.value

      // create the list of InstaceDetail with at given region and current instance type
      val instanceDetails = sizes.foldLeft(List[AwsInstance]())((sizeResult, sizeBranch) => {
        val awsInstance = parseSizeBranchJs(sizeBranch, region)
        if (awsInstance != null) {
          awsInstance :: sizeResult
        } else {
          sizeResult
        }
      })
      return instanceDetails
    } else {
      return List[AwsInstance]()
    }
  }

  private def parseSizeBranchJs(sizeBranch: JsValue, region: String): AwsInstance = {
    val nameJsResult = (sizeBranch \ "size").validate[String]
    val vCPUJsResult = (sizeBranch \ "vCPU").validate[String]
    val eCUJsResult = (sizeBranch \ "ECU").validate[String]
    val memoryGiBJsResult = (sizeBranch \ "memoryGiB").validate[String]
    val storageGBJsResult = (sizeBranch \ "storageGB").validate[String]
    val valColumnsJsResult = (sizeBranch \ "valueColumns").validate[JsArray]

    if (nameJsResult.isSuccess && vCPUJsResult.isSuccess && eCUJsResult.isSuccess &&
      memoryGiBJsResult.isSuccess && storageGBJsResult.isSuccess &&
      valColumnsJsResult.isSuccess && valColumnsJsResult.get.value.size == 1) {
      val valColumns = valColumnsJsResult.get.value(0)
      val osJsResult = (valColumns \ "name").validate[String]
      val priceJsResult = (valColumns \ "prices" \ "USD").validate[String]
      if (nameJsResult.isSuccess && priceJsResult.isSuccess) {
        return new AwsInstance(BasicAwsInstanceFactory.converZoneStringToCloudZone(region), region,
          nameJsResult.get, BasicAwsInstanceFactory.convertOsStringToOs(osJsResult.get),
          vCPUJsResult.get.toInt, memoryGiBJsResult.get.toDouble, storageGBJsResult.get, BigDecimal(priceJsResult.get), eCUJsResult.get)
      }
    }
    null
  }

}