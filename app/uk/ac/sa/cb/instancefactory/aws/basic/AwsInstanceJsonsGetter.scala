package uk.ac.sa.cb.instancefactory.aws.basic

import uk.ac.sa.cb.utils.PageSourceRetrievingService
import play.api.libs.json.JsValue
import play.api.libs.json.Json

object AwsInstanceJsonsGetter {

  def getAwsJsons: List[JsValue] = {
    val urlAndFilePairs = List(("http://a0.awsstatic.com/pricing/1/ec2/linux-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/mswin-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/rhel-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/sles-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/mswinSQL-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/mswinSQLWeb-od.min.js", "resources/AWS_Details/linux-od.min.js"))

    val jsons = urlAndFilePairs.map(pair => {
      val pageSource = PageSourceRetrievingService.getPageSource(pair._1, pair._2)
      createJason(pageSource)
    })
    
    return jsons
  }

  private def createJason(pageSource: String): JsValue = {
    val cleanedPageSource = pageSource.replace("callback(", "").replace(")", "").replace(";", "").split("\n").last

    // remove special characters
    val stringsWithoutQuotes = cleanedPageSource.split("[,(){}:\\[\\]]+").filterNot(_.contains("\""))
      .filterNot(_ == ";")
      .filterNot(_ == "")
      .filterNot(_ == "0.01")
      .filterNot(_ == "null")
      .distinct
    val rawJsonString = stringsWithoutQuotes.foldLeft(cleanedPageSource)((result, stringWithoutQuotes) => {
      val newResult = result.replace(stringWithoutQuotes, "\"%s\"".format(stringWithoutQuotes))
      newResult
    }).replace("\"\"", "\"")
      .replace("size\"s", "sizes")
      .replace("region\"s", "regions")

    val json = Json.parse(rawJsonString)
    return json
  }
}