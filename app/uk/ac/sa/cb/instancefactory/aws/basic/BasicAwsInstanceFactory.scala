package uk.ac.sa.cb.instancefactory.aws.basic

import uk.ac.sa.cb.instancefactory.aws.AbstractAwsInstanceFactory
import uk.ac.sa.cb.model.instance.aws.AwsInstance

/**
 * Get AWS instances without detail detail matrix such as physical processor or networking performance.
 */
object BasicAwsInstanceFactory extends AbstractAwsInstanceFactory {
  override def getInstances: List[AwsInstance] = {
    val jsons = AwsInstanceJsonsGetter.getAwsJsons
    val basicAwsInstances = AwsInstanceJsonParser.parseJsonsToBasicInstances(jsons)
    return basicAwsInstances
  }
}

object BasicAwsInstanceFactoryTest extends App {
  val instances = BasicAwsInstanceFactory.getInstances
  instances.map(println(_))
}