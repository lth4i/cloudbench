package uk.ac.sa.cb.instancefactory.aws.detailmatrix

import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import uk.ac.sa.cb.model.instance.aws.AwsInstanceDetailMatrix
import uk.ac.sa.cb.utils.PageSourceRetrievingService
import org.jsoup.Jsoup

object AwsInstanceDetailMetrixGetter {

  private val INSTANCE_TYPE = 0
  private val NETWORKING_PERFORMANCE = 4
  private val PHYSICAL_PROCESSOR = 5
  private val CLOCK_SPEED = 6
  private val INTEL_AVX = 7
  private val INTEL_AVX2 = 8
  private val INTEL_TURBO = 9
  private val EBS_OPT = 10
  private val ENHANCED_NETWORKING = 11

  def getDetailMatrices:Map[String, AwsInstanceDetailMatrix] = {
    val pageSource = PageSourceRetrievingService.getPageSource("http://aws.amazon.com/ec2/instance-types/", "resources/AWS_Details/aws_instance_types.html")
    val doc = Jsoup.parse(pageSource)

    val awsTableDivs = doc >> elements(".aws-table ")

    // get <div> with the largest size
    val instanceTypesMatrix = awsTableDivs.toList.foldLeft(null: org.jsoup.nodes.Element)((result, currentElement) => {
      if (result == null || result.getAllElements().size < currentElement.getAllElements().size) {
        currentElement
      } else {
        result
      }
    })

    val trElements = instanceTypesMatrix.getElementsByTag("tr")

    val detailMatrices = trElements.toList.tail.foldLeft(Map[String, AwsInstanceDetailMatrix]())((result, trElement) => {
      val tdElements = trElement.getElementsByTag("td").toList
      val instanceType = tdElements(INSTANCE_TYPE).text
      val networkingPerformance = tdElements(NETWORKING_PERFORMANCE).text
      val physicalProcessor = tdElements(PHYSICAL_PROCESSOR).text
      val clockSpeed = tdElements(CLOCK_SPEED).text
      val intelAvx = if (tdElements(INTEL_AVX).text.trim == "-") false else true
      val intelAvx2 = if (tdElements(INTEL_AVX2).text.trim == "-") false else true
      val intelTurbo = if (tdElements(INTEL_TURBO).text.trim == "-") false else true
      val ebsOpt = if (tdElements(EBS_OPT).text.trim == "-") false else true
      val enhancedNetworking = if (tdElements(ENHANCED_NETWORKING).text.trim == "-") false else true

      result.updated(instanceType, new AwsInstanceDetailMatrix(instanceType, networkingPerformance,
        physicalProcessor, clockSpeed, intelAvx, intelAvx2, intelTurbo, ebsOpt, enhancedNetworking))
    })

    return detailMatrices
    
  }
}