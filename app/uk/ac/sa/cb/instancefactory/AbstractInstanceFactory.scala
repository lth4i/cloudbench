package uk.ac.sa.cb.instancefactory

import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem

trait AbstractInstanceFactory {
  def getInstances():List[Instance]
  
  def converZoneStringToCloudZone(zoneString: String): CloudZone = {
    throw new UnsupportedOperationException("Method [%s] is not supported by [%s]"
      .format(Thread.currentThread.getStackTrace()(2).getMethodName, this.getClass.getName))
  }

  def convertOsStringToOs(osString: String): OperatingSystem = {
    throw new UnsupportedOperationException("Method [%s] is not supported by [%s]"
      .format(Thread.currentThread.getStackTrace()(2).getMethodName, this.getClass.getName))
  }
}