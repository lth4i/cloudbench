package uk.ac.sa.cb.jsonutils.name

object BasicNames {
  val PROVIDER = "provider"
  val ZONE = "zone"
  val LOCATION = "location"
  val INSTANCES = "instances"

}