package uk.ac.sa.cb.jsonutils.name

object InstanceNames {
  val OPERATING_SYSTEM = "operating system"
  val TYPE = "type"
  val CPU_CORES = "cpu cores"
  val MEM = "memory"
  val STORAGE = "storage"
  val PRICE = "price"
}