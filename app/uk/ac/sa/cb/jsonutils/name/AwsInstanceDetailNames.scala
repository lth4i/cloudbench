package uk.ac.sa.cb.jsonutils.name

object AwsInstanceDetailNames {
  val STORAGE_VOLUME = "storage volume"
  val ECU = "ecu"
  val PHYSICAL_PROCESSOR = "physical processor"
  val NETWORK_PERFORMANCE = "network performance"
  val CLOCK_SPEED = "clock speed"
  val INTEL_AVX = "intel avx"
  val INTEL_AVX2 = "intel avx2"
  val INTEL_TURBO = "intel turbo"
  val EBS_OPT = "ebs opt"
  val ENHANCED_NETWORKING = "enhanced networking"
}