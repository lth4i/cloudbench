package uk.ac.sa.cb.jsonutils.tojson

import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import uk.ac.sa.cb.jsonutils.name.BasicNames
import uk.ac.sa.cb.jsonutils.tojson.instanceparser.InstanceParser
import uk.ac.sa.cb.model.instance.Instance

object InstancesToJsonParser {
  def parseInstacesToJsonString(instances: List[Instance]): JsObject = {
    
    val instanceJsObjects = instances.map(instance => {
      createJsonFromInstance(instance)
    }).toSeq
    
    val providerJsArray = JsArray(instanceJsObjects)
    val jsObject = JsObject(Seq(BasicNames.INSTANCES -> providerJsArray))
    return jsObject
  }

  private def createJsonFromInstance(instance: Instance): JsObject = {
    return JsObject(InstanceParser.parseInstanceToJson(instance))
  }
  
}