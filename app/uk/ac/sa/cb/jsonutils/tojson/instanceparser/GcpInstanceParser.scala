package uk.ac.sa.cb.jsonutils.tojson.instanceparser

import uk.ac.sa.cb.model.instance.gcp.GcpInstance
import play.api.libs.json.JsValue
import uk.ac.sa.cb.jsonutils.name.GcpInstanceDetailNames
import play.api.libs.json.JsString

object GcpInstanceParser {
  def getProviderSpecificDetails(instance: GcpInstance): Seq[(String, JsValue)] = {
    var properties = Seq[(String, JsValue)]()
    properties = properties :+ (GcpInstanceDetailNames.GCEU -> JsString(instance.gCEU))
    return properties
  }
}