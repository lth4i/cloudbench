package uk.ac.sa.cb.jsonutils.tojson.instanceparser

import uk.ac.sa.cb.model.instance.Instance
import play.api.libs.json.JsValue
import uk.ac.sa.cb.jsonutils.name.InstanceNames
import play.api.libs.json.JsString
import uk.ac.sa.cb.jsonutils.name.BasicNames
import uk.ac.sa.cb.model.instance.aws.AwsInstance
import uk.ac.sa.cb.model.instance.azure.AzureInstance
import uk.ac.sa.cb.model.instance.gcp.GcpInstance

object InstanceParser {
  def parseInstanceToJson(instance: Instance): Seq[(String, JsValue)] = {
    var properties = Seq[(String, JsValue)]()

    properties = properties :+ (BasicNames.PROVIDER -> JsString(instance.provider.providerName))
    properties = properties :+ (BasicNames.ZONE -> JsString(instance.zone.cloudZone))
    properties = properties :+ (BasicNames.LOCATION -> JsString(instance.location))

    properties = properties :+ (InstanceNames.TYPE -> JsString(instance.instanceType))
    properties = properties :+ (InstanceNames.OPERATING_SYSTEM -> JsString(instance.operatingSystem.toString))
    properties = properties :+ (InstanceNames.CPU_CORES -> JsString(instance.cores.toString))
    properties = properties :+ (InstanceNames.MEM -> JsString(instance.mem.toString))
    properties = properties :+ (InstanceNames.STORAGE -> JsString(instance.storage))
    properties = properties :+ (InstanceNames.PRICE -> JsString(instance.price.toString))

    val providerSpecificDetail = parseProviderSpecificDetails(instance)
    
    properties = properties ++ providerSpecificDetail
    
    return properties.toSeq

  }

  private def parseProviderSpecificDetails(instance: Instance): Seq[(String, JsValue)] = instance match {
    case awsInstance: AwsInstance => {
      AwsInstanceParser.getProviderSpecificDetails(awsInstance)
    }
    case azureInstance: AzureInstance => {
      Seq[(String, JsValue)]()
    }
    case gcpInstance: GcpInstance => {
      GcpInstanceParser.getProviderSpecificDetails(gcpInstance)
    }
    case _ => throw new IllegalArgumentException("Unknown instance type [%s] of instance [%s]".format(instance.getClass(), instance))
  }
}