package uk.ac.sa.cb.jsonutils.tojson.instanceparser

import uk.ac.sa.cb.model.instance.aws.AwsInstance
import play.api.libs.json.JsValue
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.jsonutils.name.AwsInstanceDetailNames
import play.api.libs.json.JsString
import play.api.libs.json.JsBoolean

object AwsInstanceParser {
  def getProviderSpecificDetails(instance: AwsInstance): Seq[(String, JsValue)] = {
    var properties = Seq[(String, JsValue)]()
    properties = properties :+ (AwsInstanceDetailNames.STORAGE_VOLUME -> JsString(instance.storageVolume.toString))
    properties = properties :+ (AwsInstanceDetailNames.ECU -> JsString(instance.ecu))
    properties = properties :+ (AwsInstanceDetailNames.NETWORK_PERFORMANCE -> JsString(instance.networkPerformance))
    properties = properties :+ (AwsInstanceDetailNames.PHYSICAL_PROCESSOR -> JsString(instance.physicalProcessor))
    properties = properties :+ (AwsInstanceDetailNames.CLOCK_SPEED -> JsString(instance.clockSpeed))
    properties = properties :+ (AwsInstanceDetailNames.INTEL_AVX -> JsBoolean(instance.isIntelAVXSupported))
    properties = properties :+ (AwsInstanceDetailNames.INTEL_AVX2 -> JsBoolean(instance.isIntelAVX2Supported))
    properties = properties :+ (AwsInstanceDetailNames.INTEL_TURBO -> JsBoolean(instance.isIntelTurboSupported))
    properties = properties :+ (AwsInstanceDetailNames.EBS_OPT -> JsBoolean(instance.isEbsOptSupported))
    properties = properties :+ (AwsInstanceDetailNames.ENHANCED_NETWORKING -> JsBoolean(instance.isEnhancedNetworkingSupported))
    return properties
  }
}