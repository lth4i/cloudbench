package uk.ac.sa.cb.jsonutils.fromjson

import play.api.libs.json.JsValue
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem
import uk.ac.sa.cb.jsonutils.name.AwsInstanceDetailNames
import play.api.libs.json.JsString
import play.api.libs.json.JsBoolean
import uk.ac.sa.cb.model.instance.aws.AwsInstance

object JsonToAwsInstanceParser {
  def parseInstance(zone: CloudZone,
    location: String,
    instanceType: String,
    operatingSystem: OperatingSystem,
    cores: String,
    mem: String,
    storage: String,
    price: BigDecimal,
    instanceJs: JsValue): AwsInstance = {

    val storageVolRes = (instanceJs \ AwsInstanceDetailNames.STORAGE_VOLUME).validate[JsString]
    val ecuRes = (instanceJs \ AwsInstanceDetailNames.ECU).validate[JsString]
    val physicalProcessorRes = (instanceJs \ AwsInstanceDetailNames.PHYSICAL_PROCESSOR).validate[JsString]
    val networkingPerformanceRes = (instanceJs \ AwsInstanceDetailNames.NETWORK_PERFORMANCE).validate[JsString]
    val clockSpeedRes = (instanceJs \ AwsInstanceDetailNames.CLOCK_SPEED).validate[JsString]
    val intelAvxRes = (instanceJs \ AwsInstanceDetailNames.INTEL_AVX).validate[JsBoolean]
    val intelAvx2Res = (instanceJs \ AwsInstanceDetailNames.INTEL_AVX2).validate[JsBoolean]
    val intelTurboRes = (instanceJs \ AwsInstanceDetailNames.INTEL_TURBO).validate[JsBoolean]
    val ebsOptRes = (instanceJs \ AwsInstanceDetailNames.EBS_OPT).validate[JsBoolean]
    val enhancedNetworkingRes = (instanceJs \ AwsInstanceDetailNames.ENHANCED_NETWORKING).validate[JsBoolean]

    if (storageVolRes.isSuccess && ecuRes.isSuccess && networkingPerformanceRes.isSuccess &&
      physicalProcessorRes.isSuccess && clockSpeedRes.isSuccess &&
      intelAvxRes.isSuccess && intelAvx2Res.isSuccess && intelTurboRes.isSuccess &&
      ebsOptRes.isSuccess && enhancedNetworkingRes.isSuccess) {

      val storageVol = storageVolRes.get.value
      val ecu = ecuRes.get.value
      val physicalProcessor = physicalProcessorRes.get.value
      val networkingPerformance = networkingPerformanceRes.get.value
      val clockSpeed = clockSpeedRes.get.value
      val intelAvx = intelAvxRes.get.value
      val intelAvx2 = intelAvx2Res.get.value
      val intelTurbo = intelTurboRes.get.value
      val ebsOpt = ebsOptRes.get.value
      val enhancedNetworking = enhancedNetworkingRes.get.value

      return new AwsInstance(zone,
        location,
        instanceType,
        operatingSystem,
        cores.toInt,
        mem.toDouble,
        storage,
        price,
        ecu,
        networkingPerformance,
        physicalProcessor,
        clockSpeed,
        intelAvx,
        intelAvx2,
        intelTurbo,
        ebsOpt,
        enhancedNetworking)
    } else {
      throw new IllegalArgumentException("Cannot parse [%s] to [AwsInstance]".format(instanceJs))
    }
  }
}