package uk.ac.sa.cb.jsonutils.fromjson

import play.api.libs.json.Json
import play.api.libs.json.JsValue
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.jsonutils.name.BasicNames
import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import uk.ac.sa.cb.jsonutils.name.InstanceNames
import uk.ac.sa.cb.label.provider.CloudProvider
import uk.ac.sa.cb.label.zone.CloudZone
import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import uk.ac.sa.cb.label.os.OperatingSystem
import uk.ac.sa.cb.label.provider._
import uk.ac.sa.cb.model.instance.azure.AzureInstance

object JsonToInstancesParser {
  def createInstancesFromJson(jsonString: String): List[Instance] = {
    val json = Json.parse(jsonString)

    val instancesRes = (json \ BasicNames.INSTANCES).validate[JsArray]

    if (instancesRes.isSuccess) {
      val instancesJs = instancesRes.get.value
      val instances = instancesJs.map(instanceJs => {
        try {
          parseInstance(instanceJs)
        } catch {
          case e: IllegalAccessException => {
            println("Failed to parse [%s] because [%s]".format(instanceJs, e.getMessage()))
            null
          }
        }
      }).toList.filter(_ != null)
      return instances
    }

    throw new IllegalArgumentException("Cannot parse json string into Instances")
  }

  private def parseInstance(instanceJs: JsValue): Instance = {
    val providerRes = (instanceJs \ BasicNames.PROVIDER).validate[JsString]
    val zoneRes = (instanceJs \ BasicNames.ZONE).validate[JsString]
    val locationRes = (instanceJs \ BasicNames.LOCATION).validate[JsString]
    val typeRes = (instanceJs \ InstanceNames.TYPE).validate[JsString]
    val osRes = (instanceJs \ InstanceNames.OPERATING_SYSTEM).validate[JsString]
    val cpuCoresRes = (instanceJs \ InstanceNames.CPU_CORES).validate[JsString]
    val memRes = (instanceJs \ InstanceNames.MEM).validate[JsString]
    val storageRes = (instanceJs \ InstanceNames.STORAGE).validate[JsString]
    val priceRes = (instanceJs \ InstanceNames.PRICE).validate[JsString]

    if (providerRes.isSuccess && zoneRes.isSuccess && locationRes.isSuccess &&
      typeRes.isSuccess && osRes.isSuccess &&  cpuCoresRes.isSuccess &&
      memRes.isSuccess && storageRes.isSuccess && priceRes.isSuccess) {

      val provider = CloudProvider.getProviderFromString(providerRes.get.value)
      val zone = CloudZone.getZoneFromString(zoneRes.get.value)
      val location = locationRes.get.value
      val instanceType = typeRes.get.value
      val os = OperatingSystem.getOperatingSystemFromString(osRes.get.value)
      val cpuCores = cpuCoresRes.get.value
      val mem = memRes.get.value
      val storage = storageRes.get.value
      val price = BigDecimal(priceRes.get.value)

      val instance: Instance = provider match {
        case AWS => {
          JsonToAwsInstanceParser.parseInstance(zone, location, instanceType, os, cpuCores, mem, storage, price, instanceJs)
        }
        case AZURE => {
          new AzureInstance(zone, location, instanceType, os, cpuCores.toInt, mem.replace("GB", "").toDouble, storage, price)
        }
        case GCP => {
          JsonToGcpInstanceParser.parseInstance(zone, location, instanceType, os, cpuCores, mem, storage, price, instanceJs)
        }
        case _ => throw new IllegalArgumentException("Unknown provider [%s]".format(provider))
      }
      return instance
    } else {
      throw new IllegalArgumentException("Cannot parse [%s] to any instance".format(instanceJs))
    }

  }
}