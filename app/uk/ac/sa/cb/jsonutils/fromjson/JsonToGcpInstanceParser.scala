package uk.ac.sa.cb.jsonutils.fromjson

import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem
import play.api.libs.json.JsValue
import uk.ac.sa.cb.model.instance.gcp.GcpInstance
import uk.ac.sa.cb.jsonutils.name.GcpInstanceDetailNames
import play.api.libs.json.JsString

object JsonToGcpInstanceParser {
  def parseInstance(zone: CloudZone,
    location: String,
    instanceType: String,
    operatingSystem: OperatingSystem,
    cores: String,
    mem: String,
    storage: String,
    price: BigDecimal,
    instanceJs: JsValue): GcpInstance = {

    val gceuRes = (instanceJs \ GcpInstanceDetailNames.GCEU).validate[JsString]

    if (gceuRes.isSuccess) {

      val gceu = gceuRes.get.value

      return new GcpInstance(zone,
        location,
        instanceType,
        operatingSystem,
        cores.toInt,
        mem.toDouble,
        storage,
        price,
        gceu)
    } else {
      throw new IllegalArgumentException("Cannot parse [%s] to [AwsInstance]".format(instanceJs))
    }
  }

}