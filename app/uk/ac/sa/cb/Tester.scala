package uk.ac.sa.cb

import uk.ac.sa.cb.instancefactory.InstanceFactory
import uk.ac.sa.cb.jsonutils.tojson.InstancesToJsonParser
import uk.ac.sa.cb.persistence.PersistenceManager
import uk.ac.sa.cb.jsonutils.fromjson.JsonToInstancesParser
import uk.ac.sa.cb.cache.InstancesCache
import uk.ac.sa.cb.instancefactory.gcp.GcpFactory
import uk.ac.sa.cb.instancefactory.azure.AzureInstanceFactory
import uk.ac.sa.cb.instancefactory.aws.AwsInstanceFactory

object Tester extends App {

//  def writeData = {
//    val instances = InstanceFactory.getInstances
//    instances.map(println(_))
//    val jsObject = InstancesToJsonParser.parseInstacesToJsonString(instances)
//
//    PersistenceManager.writeJsonToFile(jsObject)
//  }
//
//  def readData = {
//    val dataString = PersistenceManager.readJsonFileToString
//    val instances = JsonToInstancesParser.createInstancesFromJson(dataString)
//    instances.map(println(_))
//    println(instances.size)
//
//  }
//
//  readData
  //  println(dataString)
  //  val instances = InstancesCache.getInstances
  //  instances.map(i => println(i.getDetailedInstanceInfo))
//  GcpFactory.getInstances.map(println(_))
//  AzureInstanceFactory.getInstances.map(println(_))
  AwsInstanceFactory.getInstances.map(println(_))
}