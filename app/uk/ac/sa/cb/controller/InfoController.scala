package uk.ac.sa.cb.controller

import play.api.mvc.Controller
import play.api.mvc.Action

object InfoController extends Controller {
  def showAbout = Action { implicit request =>
    Ok(uk.ac.sa.cb.views.html.about())
  }
}