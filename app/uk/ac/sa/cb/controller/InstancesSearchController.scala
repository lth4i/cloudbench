package uk.ac.sa.cb.controller

import play.api.data.Form
import play.api.data.Forms.{ list, mapping, longNumber, nonEmptyText, text, boolean, number }
import play.api.mvc.Action
import play.api.mvc.Controller
import uk.ac.sa.cb.search.InstancesSearchService
import uk.ac.sa.cb.model.search.SearchCriterion
import uk.ac.sa.cb.model.search.SearchCriteria
import play.api.mvc.Flash
import uk.ac.sa.cb.controller.form.InstancesSearchFormPreparer
import uk.ac.sa.cb.search.criteria.InstanceSearchCriteria
import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.model.search.SearchResult

object InstancesSearchController extends Controller {

  def searchInstancesFormDisplay = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined) instancesSearchForm else instancesSearchForm
    val formData = InstancesSearchFormPreparer.getInstancesSearchFormData
    Ok(uk.ac.sa.cb.views.html.instancesSearch(form, formData))
  }

  def validateCreterion(criterion: String, sign: String, userGivenVal: String) = criterion match {
    case InstanceSearchCriteria.CORES | InstanceSearchCriteria.MEM | InstanceSearchCriteria.STORAGE | InstanceSearchCriteria.PRICE => {
      if (sign.trim() != "") {
        try {
          BigDecimal(userGivenVal)
        } catch {
          case _: NumberFormatException => { // failed to convert user given value to number

          }
        }
      }
    }
  }

  private val instancesSearchForm: Form[SearchCriteria] = Form(
    mapping(
      "criteria" -> list(mapping(
        "criterionLabel" -> nonEmptyText,
        "signString" -> text,
        "userGivenVal" -> text)(SearchCriterion.apply)(SearchCriterion.unapply)),
      "sortLabel" -> nonEmptyText,
      "isDescending" -> boolean,
      "pageNumber" -> number)(SearchCriteria.apply)(SearchCriteria.unapply))

  def searchInstances = Action { implicit request =>
    val search = instancesSearchForm.bindFromRequest
    search.fold(hasErrors = {
      form =>
        Redirect(uk.ac.sa.cb.controller.routes.InstancesSearchController.searchInstancesFormDisplay).flashing(Flash(form.data) +
          ("errors" -> form.errors.mkString(", ")))
    }, success = {
      searchCriteria =>
        {
                    
          val criteria = searchCriteria.criteria.filterNot(c => c.signString == "" || c.userGivenVal == "")

          val invalidSearches = criteria.filter(searchCriterion => {
            searchCriterion.criterionLabel match {
              case InstanceSearchCriteria.CORES | InstanceSearchCriteria.MEM | InstanceSearchCriteria.STORAGE | InstanceSearchCriteria.PRICE => {
                if (searchCriterion.userGivenVal.trim() != "") {
                  try {
                    BigDecimal(searchCriterion.userGivenVal) < 0
                  } catch {
                    case _: NumberFormatException => {
                      true
                    }
                  }
                } else {
                  false
                }
              }
              case _ => false
            }
          })

          if (invalidSearches.isEmpty) {
            
            val instances = InstancesSearchService.searchInstances(criteria)
            
            val totalPages = Math.ceil(instances.size / 10.0).toInt
            var pageNumber = searchCriteria.pageNumber
            if (pageNumber > totalPages) {
              pageNumber = totalPages
            }
            val returnedInstances = processInstanceSearchResult(instances, searchCriteria.sortLabel,
                searchCriteria.isDescending, searchCriteria.pageNumber)

            val searchResult = new SearchResult(returnedInstances, request.host, request.uri, searchCriteria.sortLabel,
                searchCriteria.isDescending, searchCriteria.pageNumber, totalPages)
                
            Ok(uk.ac.sa.cb.views.html.instancesSearchResult(searchResult))
          } else {
            val flash = invalidSearches.foldLeft(Flash(search.data))((currentFlash, item) => {
              val label = item.criterionLabel
              currentFlash + ("%s.error".format(label) -> "%s must be a non negative number".format(label))
            }) + ("error" -> "error")
            Redirect(uk.ac.sa.cb.controller.routes.InstancesSearchController.searchInstancesFormDisplay).flashing(flash)
          }
        }
    })
  }

  def processInstanceSearchResult(allInstances: List[Instance], sortLabel: String, isDescending: Boolean, pageNumber: Int): List[Instance] = {
    val NUM_ITEMS_PER_PAGE = 10
    val startIndex = (pageNumber - 1) * NUM_ITEMS_PER_PAGE
    var endIndex = pageNumber * NUM_ITEMS_PER_PAGE
    if (endIndex > allInstances.size) {
      endIndex = allInstances.size
    }

    var sortedInstances = sortLabel match {
      case "Provider" => allInstances.sortBy(instance => instance.provider)
      case "Zone" => allInstances.sortBy(instance => instance.zone)
      case "Location" => allInstances.sortBy(instance => instance.location)
      case "Operating System" => allInstances.sortBy(instance => instance.operatingSystem)
      case "Instance Type" => allInstances.sortBy(instance => instance.instanceType)
      case "CPU Cores" => allInstances.sortBy(instance => instance.cores)
      case "Memory (GB)" => allInstances.sortBy(instance => instance.mem)
      case "Storage" => allInstances.sortBy(instance => instance.storage)
      case "Storage Volume (GB)" => allInstances.sortBy(instance => instance.storageVolume)
      case "Price ($/hr)" => allInstances.sortBy(instance => instance.price)
      case _ => throw new IllegalArgumentException("Unknown column named [%s]".format(sortLabel))
    }

    if (isDescending) {
      sortedInstances = sortedInstances.reverse
    }

    val returnedInstances = sortedInstances.slice(startIndex, endIndex)

    return returnedInstances
  }

}