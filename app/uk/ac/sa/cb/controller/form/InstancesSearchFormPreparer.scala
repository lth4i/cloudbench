package uk.ac.sa.cb.controller.form

import uk.ac.sa.cb.search.criteria.InstanceSearchCriteria
import uk.ac.sa.cb.label.provider.CloudProvider
import uk.ac.sa.cb.search.sign._
import uk.ac.sa.cb.label.zone.CloudZone
import uk.ac.sa.cb.label.os.OperatingSystem

object InstancesSearchFormPreparer {
  def getInstancesSearchFormData: InstancesSearchFormData = {
    val providers = (InstanceSearchCriteria.PROVIDER, List(StringExactEqualSign).map(_.sign), CloudProvider.allProviders.map(_.toString), "Select Cloud Provider")
    val zones = (InstanceSearchCriteria.ZONE, List(StringExactEqualSign).map(_.sign), CloudZone.allZones, "Select Geographical Zone")
    val oses = (InstanceSearchCriteria.OS, List(StringExactEqualSign).map(_.sign), OperatingSystem.operatingSystemsForSearch.map(_.toString), "Select Operating System")
    val core = (InstanceSearchCriteria.CORES,
      List(NumberEqualSign, NumberLessOrEqualSign, NumberLessSign, NumberLargerOrEqualSign, NumberLargerSign).map(_.sign), null, "Enter a number of CPU Cores in numeric value")
    val mem = (InstanceSearchCriteria.MEM,
      List(NumberEqualSign, NumberLessOrEqualSign, NumberLessSign, NumberLargerOrEqualSign, NumberLargerSign).map(_.sign), null, "Enter memory capacity (Gigabytes) in numeric value")
    val storage = (InstanceSearchCriteria.STORAGE,
      List(NumberEqualSign, NumberLessOrEqualSign, NumberLessSign, NumberLargerOrEqualSign, NumberLargerSign).map(_.sign), null, "Enter storage capacity (Gigabytes) in numeric value")
    val price = (InstanceSearchCriteria.PRICE,
      List(NumberEqualSign, NumberLessOrEqualSign, NumberLessSign, NumberLargerOrEqualSign, NumberLargerSign).map(_.sign), null, "Enter price (in US dollar) per hour in numeric value")
      
    return new InstancesSearchFormData(List(providers, zones, oses, core, mem, storage, price))
  }
}

case class InstancesSearchFormData(formData: List[(String, List[String], List[String], String)])