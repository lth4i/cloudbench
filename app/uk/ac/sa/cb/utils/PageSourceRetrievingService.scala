package uk.ac.sa.cb.utils

import scala.io.Source

object PageSourceRetrievingService {

  def getPageSource(url: String, filePath: String): String = {
    val pageSource = getPageSourceFromUrl(url)
    if (pageSource != null) {
      return pageSource
    } else {
      return getPageSourceFromFile(filePath)
    }
  }

  private def getPageSourceFromUrl(url: String): String = {
    try {
      val pageSource = Source.fromURL(url).mkString
      return pageSource
    } catch {
      case e: Exception => {
        println(e.getMessage())
        return null
      }
    }
  }

  private def getPageSourceFromFile(filePath: String): String = {
    return Source.fromFile(filePath).mkString
  }
}