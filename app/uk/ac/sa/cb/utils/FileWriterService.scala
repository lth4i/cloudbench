package uk.ac.sa.cb.utils

import java.io.File
import java.io.PrintWriter

object FileWriterService {
  def writeToNewFile(fileName: String, fileContent: String) = {
    val f = new File(fileName)
    if (f.exists()) {
      f.delete()
    }
    val printer = new PrintWriter(f)
    printer.write(fileContent)
    printer.close
  }
}