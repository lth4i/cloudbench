package uk.ac.sa.cb.cache

import uk.ac.sa.cb.model.instance.Instance
import uk.ac.sa.cb.persistence.PersistenceManager
import uk.ac.sa.cb.jsonutils.fromjson.JsonToInstancesParser

object InstancesCache {
  private val instances = scala.collection.mutable.MutableList[Instance]()
  updateCache(
    JsonToInstancesParser.createInstancesFromJson(
      PersistenceManager.readJsonFileToString))
  println("Finish initialising instance cache with [%s] instances".format(instances.size))
  
//  val locations = instances.map(_.location)
//  
//  locations.sortBy(l => l).map(println(_))

  def updateCache(newInstances: List[Instance]) = {
    instances.synchronized {
      instances.clear
      instances ++= newInstances
    }
  }

  def getInstances = {
    instances.toList // return a new immutable list of instances
  }
}

//object T extends App {
//  InstancesCache.getInstances
//}