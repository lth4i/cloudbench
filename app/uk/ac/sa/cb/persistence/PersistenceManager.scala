package uk.ac.sa.cb.persistence

import play.api.libs.json.JsObject
import play.api.libs.json.Json
import java.io.File
import java.io.PrintWriter
import uk.ac.sa.cb.utils.FileWriterService

object PersistenceManager {
  private val FILE_NAME = "resources/data.json"

  def writeJsonToFile(jsObject: JsObject) = {
    this.synchronized {
      val jsString = Json.prettyPrint(jsObject)
      FileWriterService.writeToNewFile(FILE_NAME, jsString)
    }
  }

  def readJsonFileToString: String = {
    this.synchronized {
      return scala.io.Source.fromFile(FILE_NAME).mkString
    }
  }
}