package controllers

import play.api.mvc.Controller
import play.api.mvc.Action
import models.Instance
import forms.WeightsUpdateForm
import play.api.mvc.Flash
import logic.updater.WeightsUpdating
import models.InstancesSelectingResult
import logic.selector.InstancesSelecting
import play.api.Logger
import java.io.FileInputStream
import java.io.ObjectInputStream
import java.io.DataInputStream
import java.io.BufferedReader
import java.io.InputStreamReader

object Admin extends Controller with Secured {
  def list = withAuth { username =>
    implicit request => {
      val instances = Instance.findAll
      Ok(views.html.admin.update(instances))
    }
  }

  def updateWeights = Action { implicit request =>
    //    return ("OK1", "OK2", "OK3")
    val newWeightsForm = WeightsUpdateForm.weightsUpdateForm.bindFromRequest
    newWeightsForm.fold(
      hasErrors = { form =>
        {
          //          val errorMsg =
          //            if (form.data("firstSelection") == "" || form.data("secondSelection") == "" || form.data("thirdSelection") == "")
          //              "All selection must selected"
          //            else
          //              "All weights cannot be 0"
          Redirect(routes.Admin.list()).
            flashing(Flash(form.data) + ("error" -> form.errors(0).message))
        }
      },
      success = { weights =>
        {
          val w1 = weights.w1
          val w2 = weights.w2
          val w3 = weights.w3
          val w4 = weights.w4
          val w5 = if (weights.w5) 1 else 0
          val firstSelection = weights.firstSelection
          val secondSelection = weights.secondSelection
          val thirdSelection = weights.thirdSelection

          WeightsUpdating.updateWeights(w1, w2, w3, w4, w5, firstSelection, secondSelection, thirdSelection)
          val result = InstancesSelectingResult(w1, w2, w3, w4, w5, InstancesSelecting.getInstances(w1, w2, w3, w4, w5))
          //          Redirect(routes.InstancesSelector.displayInstancesSelectingResult(result))
          Ok(views.html.select.result(result))
        }
      })

    //    Ok(List("OK1", "OK2", "OK3"))
  }

  def upload = Action(parse.multipartFormData) { request =>
    request.body.file("Rankings").map { file =>
      import java.io.File
      import java.util.Scanner
      val filename = file.filename

      val fi = new BufferedReader(new InputStreamReader(new FileInputStream(file.ref.file)))

      val lines = scala.collection.mutable.MutableList[String]()
      var line: String = null
      while ({ line = fi.readLine(); line } != null) {
        if (line.trim != "") {
          lines += line
        }
      }

      try {
        WeightsUpdating.upload(lines.toList)
        Redirect(routes.Admin.list).flashing(
          "success" -> "Successfully upload file %s".format(filename))
      } catch {
        case e: IllegalArgumentException => {
          Redirect(routes.Admin.list).flashing(
            "error" -> e.getMessage())
        }
      }
      //    picture.ref.moveTo(new File("/tmp/picture"))
    }.getOrElse {
      Redirect(routes.Admin.list).flashing(
        "error" -> "Missing file")
    }
  }

  //  public static Result upload () {
  //    MultipartFormData body = request().body().asMultipartFormData();
  //    FilePart picture = body.getFile("picture");
  //    if (picture != null) {
  //      String fileName = picture.getFilename();
  //      String contentType = picture.getContentType();
  //      File file = picture.getFile();
  //      return ok("File uploaded");
  //    } else {
  //      flash("error", "Missing file");
  //      return redirect(routes.Application.index());
  //    }
  //  }
}