package controllers

import play.api.mvc.Controller
import play.api.mvc.Action
import logic.cloudinstance.DetailGetter
import models.InstanceDetail

object Details extends Controller {
  
  var details = List[InstanceDetail]()
  
  def getAwsDetails = Action { implicit request => {
    if (details.length == 0) {
      details = DetailGetter.getAllAwsDetails
    }
    Ok(views.html.details.awsDetails(details))
  }
  
    
  }
}