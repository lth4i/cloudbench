package controllers

import play.api.mvc.Controller
import play.api.mvc.Action
import forms.WeightsForm
import logic.selector.InstancesSelecting
import models.InstancesSelectingResult
import play.api.mvc.Flash
import play.Logger

object InstancesSelector extends Controller {

  def getWeights = Action { implicit request =>
    Ok(views.html.select.weights())
  }

  def getInstances() = Action { implicit request =>
    //    return ("OK1", "OK2", "OK3")
    val newWeightsForm = WeightsForm.weightsForm.bindFromRequest
    newWeightsForm.fold(
      hasErrors = { form =>
        {
          Redirect(routes.InstancesSelector.getWeights()).
            flashing(Flash(form.data) + ("error" -> "All weights cannot be zeros"))
        }
      },
      success = { weights =>
        {
          val w1 = weights.w1
          val w2 = weights.w2
          val w3 = weights.w3
          val w4 = weights.w4
          val w5 = if (weights.w5) 1 else 0

          val instanceTypes = InstancesSelecting.getInstances(w1, w2, w3, w4, w5)
          val result = InstancesSelectingResult(w1, w2, w3, w4, w5, instanceTypes)
          //          Redirect(routes.InstancesSelector.displayInstancesSelectingResult(result))
          Ok(views.html.select.result(result))
        }
      })

    //    Ok(List("OK1", "OK2", "OK3"))
  }
}