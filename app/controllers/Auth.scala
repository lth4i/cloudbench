package controllers

import play.api.mvc.Controller
import play.api.data.Form
import play.api.data.Forms.text
import play.api.data.Forms.tuple
import play.api.mvc.Action
import play.api.mvc.Security
import play.api.mvc.Flash

object Auth extends Controller {
  val loginForm = Form(
    tuple(
      "username" -> text,
      "password" -> text) verifying ("Invalid username or password", result => result match {
        case (username, password) => check(username, password)
      }))

  def check(username: String, password: String) = {
    (username == "admin" && password == "1234")
  }

  def login = Action { implicit request =>
    Ok(views.html.admin.login(null))
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        val errorMsg = formWithErrors.errors(0).message
//        BadRequest(views.html.admin.login(formWithErrors))
        Redirect(routes.Auth.login()).
                  flashing(Flash(formWithErrors.data) + ("error" -> errorMsg))
      },

      //      hasErrors = { form =>
      //        {
      //          Redirect(routes.Auth.login()).
      //            flashing(Flash(form.data) + ("error" -> "Invalid username and password"))
      //        }
      //      },
      user => Redirect(routes.Admin.list).withSession(Security.username -> user._1))
  }

  def logout = Action {
    Redirect(routes.Auth.login).withNewSession.flashing(
      "success" -> "You are now logged out.")
  }
}