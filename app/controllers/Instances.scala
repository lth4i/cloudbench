package controllers

import play.api.mvc.{ Controller, Action }

import play.api.data.Form
import play.api.data.Forms.{ mapping, longNumber, nonEmptyText }

import play.api.mvc.Flash

import models.Instance

object Instances extends Controller {
  def list = Action { implicit request =>
    val instances = Instance.findAll
    Ok(views.html.instances.list(instances))
  }

  def show(instanceType: String) = Action { implicit request =>
    Instance.findByType(instanceType).map { instance =>
      Ok(views.html.instances.details(instance))
    }.getOrElse(NotFound)
  }
  
  private val instanceForm: Form[Instance] = Form(
    mapping(
      "instanceType" -> nonEmptyText.verifying("Duplicate instance type", Instance.findByType(_).isEmpty),
      "vCPU" -> longNumber,
      "memory" -> nonEmptyText,
      "processor" -> nonEmptyText,
      "clockSpeed" -> nonEmptyText
    )(Instance.apply)(Instance.unapply)
  )
  
  def newInstance = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      instanceForm.bind(request.flash.data)
    else
      instanceForm
    Ok(views.html.instances.editInstance(form))
  }
  
//  def save = Action { implicit request =>
//    val newInstanceForm = instanceForm.bindFromRequest()
//    
//    newInstanceForm.fold (
//	    hasErrors =  { form => 
//	      Redirect(routes.Instances.newInstance()).
//	        flashing(Flash(form.data) + ("error" -> "Validation Error"))
//	    },
//	    success = { newInstance =>
//	      Instance.add(newInstance)
//	      val message = "Successfully added instance %s".format(newInstance.instanceType)
//	      Redirect(routes.Instances.show(newInstance.instanceType)).
//	      	flashing("success" -> message)
//	    }
//    )
//  }
}