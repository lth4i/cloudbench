package models

case class WeightsUpdate(w1:Long, w2:Long, w3:Long, w4:Long, w5:Boolean, firstSelection:String, secondSelection:String, thirdSelection:String)