package models

case class Instance(instanceType: String, vCPU:Long, memory:String, processor:String, clockSpeed:String)

object Instance {
  var instances = Set(
    Instance("m1.xlarge", 4, "15.0", "Intel Xeon E5-2650", "2.00"),
    Instance("m2.xlarge", 2, "17.1", "Intel Xeon E5-2665", "2.40"),
    Instance("m2.2xlarge", 4, "34.2", "Intel Xeon E5-2665", "2.40"),
    Instance("m2.4xlarge", 8, "68.4", "Intel Xeon E5-2665", "2.40"),
    Instance("m3.xlarge", 4, "15.0", "Intel Xeon E5-2670", "2.60"),
    Instance("m3.2xlarge", 8, "30.0", "Intel Xeon E5-2670", "2.60"),
    Instance("cr1.8xlarge", 32, "244.0", "Intel Xeon E5-2670", "2.60"),
    Instance("cc1.4xlarge", 16, "23.0", "Intel Xeon X5570", "2.93"),
    Instance("cc2.8xlarge", 32, "60.5", "Intel Xeon X5570", "2.93"),
    Instance("hi1.4xlarge", 16, "60.5", "Intel Xeon E5620", "2.40"),
    Instance("hs1.8xlarge", 16, "117.0", "Intel Xeon E5-2650", "2.00"),
    Instance("cg1.4xlarge", 16, "22.5", "Intel Xeon X5570", "2.93")
  )
    
  def findAll = instances.toList
  
  def findByType(instanceType:String) = instances.find(_.instanceType == instanceType)
  
  def add(instance: Instance) = {
    instances = instances + instance
  }
}

object Test extends App {
  val types = Instance.instances.map(i => {
    "%s".format(i.instanceType) 
  })
  println(types.mkString(" "))
}