package models

case class InstancesSelectingResult(w1:Long, w2:Long, w3:Long, w4:Long, w5:Long,
    instanceTypes:List[Instance])