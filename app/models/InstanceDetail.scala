package models

class InstanceDetail(val region: String, val instanceType: String,
  val size: String, val vCPU: String, val eCU: String, val memoryGiB: String,
  val storageGB: String, val name: String, val price: String) extends Equals {
  def canEqual(other: Any) = {
    other.isInstanceOf[models.InstanceDetail]
  }

  override def toString: String = {
    return "[%s,%s,%s,%s,%s,%s,%s,%s,%s]".format(region, instanceType, size, vCPU, eCU, memoryGiB, storageGB, name, price)
  }

  override def equals(other: Any) = {
    other match {
      case that: models.InstanceDetail => that.canEqual(InstanceDetail.this) &&
        region == that.region && instanceType == that.instanceType && size == that.size && name == that.name
      case _ => false
    }
  }

  override def hashCode() = {
    val prime = 41
    prime * (prime * (prime * (prime + region.hashCode) + instanceType.hashCode) + size.hashCode) + name.hashCode
  }
}