package logic.selector

import models.Instance

object InstancesSelecting {
  private val lines = scala.io.Source.fromFile("resources/rank.out").mkString.split("\n")
  
  val instancesMap = scala.collection.mutable.Map[(Long, Long, Long, Long, Long),
    List[Instance]]()
  
  lines.map(l => {
    val tokens = l.split("\t").toList
    instancesMap += ((tokens(0).toLong, tokens(1).toLong, tokens(2).toLong, tokens(3).toLong, tokens(4).toLong)
        -> tokens.drop(5).map(instanceType => Instance.findByType(instanceType).get))
  })
    
  def getInstances(w1:Long, w2:Long, w3:Long, w4:Long, w5:Long):List[Instance] = {
    return instancesMap((w1, w2, w3, w4, w5))
  }
}