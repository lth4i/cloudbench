package logic.updater

import logic.selector.InstancesSelecting
import java.io.FileWriter
import models.Instance

object WeightsUpdating {

  def updateWeights(w1: Long, w2: Long, w3: Long, w4: Long, w5: Long,
    firstSelectionString: String, secondSelectionString: String, thirdSelectionString: String) = {
    val currentSelections = InstancesSelecting.getInstances(w1, w2, w3, w4, w5)

    val firstSelection = Instance.findByType(firstSelectionString).get
    val secondSelection = Instance.findByType(secondSelectionString).get
    val thirdSelection = Instance.findByType(thirdSelectionString).get
    
    if (firstSelection == currentSelections(0)
      && secondSelection == currentSelections(1)
      && thirdSelection == currentSelections(2)) {
      // nothing change...
    } else {
      // update cache
      val newSelections = List(firstSelection, secondSelection, thirdSelection) ++ currentSelections.drop(3)
      InstancesSelecting.instancesMap += ((w1, w2, w3, w4, w5) -> newSelections)

      // update file
      updateFile
    }
  }

  def upload(lines: List[String]) = {
    // validation
    val instances = Instance.findAll.map(i => {
      i.instanceType
    }).toList
        
    val newSelections = lines.map(line => {
      val tokens = line.split("\t")
      if (tokens.length < 8) {
        throw new IllegalArgumentException("Invalid line: [%s]".format(line))
      }
      try {
        val w1 = tokens(0).toLong
        val w2 = tokens(1).toLong
        val w3 = tokens(2).toLong
        val w4 = tokens(3).toLong
        val w5 = tokens(4).toLong

        // check range of all weights
        if ((w1 < 0 || w1 > 5)
          || (w2 < 0 || w2 > 5)
          || (w3 < 0 || w3 > 5)
          || (w4 < 0 || w4 > 5)
          || (w5 < 0 || w5 > 1)) {
          throw new IllegalArgumentException("Invalid line: [%s]".format(line))
        }

        // make sure not all of them is 0
        if (w1 + w2 + w3 + w4 == 0) {
          throw new IllegalArgumentException("Invalid line: [%s]. The sum of first 4 weights has to be 0".format(line))
        }

        val types = tokens.drop(5).toList
        types.map(t => {
          // check instance is availability
          if (!instances.contains(t.trim)) {
            throw new IllegalArgumentException("Invalid line: [%s]. Unknown instance [%s]".format(line, t))
          }
        })
        (w1, w2, w3, w4, w5, types)
      } catch {
        case e: NumberFormatException => {
          throw new IllegalArgumentException("Invalid line: [%s]".format(line))
        }
        case e: Exception => {
          throw e
        }
      }
    })

    // update cache
    newSelections.map(s => {
      InstancesSelecting.instancesMap += ((s._1, s._2, s._3, s._4, s._5) -> s._6.map(instanceType => Instance.findByType(instanceType).get))
    })

    // update file
    updateFile
  }

  def updateFile = {
    val fw = new FileWriter("resources/rank.out", false)
    InstancesSelecting.instancesMap.map(item => {
      fw.write("%s\t%s\t%s\t%s\t%s\t%s\n"
        .format(item._1._1, item._1._2, item._1._3, item._1._4, item._1._5, item._2.mkString("\t")))
    })
    fw.flush()
    fw.close()
  }

}