package logic.cloudinstance

import scala.io.Source
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.JsArray
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.JsonNode
import models.InstanceDetail

object DetailGetter {

  private def getJson(locPairs: (String, String)): JsValue = {
    val (url, loc) = locPairs
    val urlContent = getFromUrl(url)

    // .replace("callback(", "").replace(")", "").replace(";", "").split("\n").last
    val raw = {
      if (urlContent != null) urlContent // try to get data from the website
      else Source.fromFile(loc).mkString // if fail, get it from the local file
    }.replace("callback(", "").replace(")", "").replace(";", "").split("\n").last

    val stringsWithoutQuotes = raw.split("[,(){}:\\[\\]]+").filterNot(_.contains("\""))
      .filterNot(_ == ";")
      .filterNot(_ == "")
      .filterNot(_ == "0.01")
      .filterNot(_ == "null")
      .distinct
    val rawJsonString = stringsWithoutQuotes.foldLeft(raw)((result, s) => {
      val newResult = result.replace(s, "\"%s\"".format(s))
      newResult
    }).replace("\"\"", "\"")
      .replace("size\"s", "sizes")
      .replace("region\"s", "regions")

    return Json.parse(rawJsonString)
  }

  private def getFromUrl(url: String): String = {
    try {
      val html = Source.fromURL(url)
      val raw = html.mkString
      return raw
    } catch {
      case e: Exception => {
        println(e.getMessage())
        return null
      }
    }
  }

  def getAwsDetail(locPairs: (String, String)): List[InstanceDetail] = {
    val json: JsValue = getJson(locPairs)
    //    println(json)

    // go through json tree to find details
    val regionsJsResult = (json \ "config" \ "regions").validate[JsArray] // get regions
    if (regionsJsResult.isSuccess) {
      val regions = regionsJsResult.get.value

      val regionsMap = regions.foldLeft(List[InstanceDetail]())((regionResult, regionBranch) => {
        val regionJsResult = (regionBranch \ "region").validate[String] // get region name
        val instanceTypesJsResult = (regionBranch \ "instanceTypes").validate[JsArray] // get instance types branch of region
        if (regionJsResult.isSuccess && instanceTypesJsResult.isSuccess) {
          val region = regionJsResult.get
          val instanceTypes = instanceTypesJsResult.get.value

          val instanceTypesMap = instanceTypes.foldLeft(List[InstanceDetail]())((instanceTypeResult, typeBranch) => {
            val typeJsResult = (typeBranch \ "type").validate[String] // get instance type
            val sizesJsResult = (typeBranch \ "sizes").validate[JsArray] // get instance sizes of type
            if (typeJsResult.isSuccess && sizesJsResult.isSuccess) {
              val instanceType = typeJsResult.get
              val sizes = sizesJsResult.get.value

              // create the list of InstaceDetail with at given region and current instance type
              val instanceDetails = sizes.foldLeft(List[InstanceDetail]())((sizeResult, sizeBranch) => {
                val sizeJsResult = (sizeBranch \ "size").validate[String]
                val vCPUJsResult = (sizeBranch \ "vCPU").validate[String]
                val eCUJsResult = (sizeBranch \ "ECU").validate[String]
                val memoryGiBJsResult = (sizeBranch \ "memoryGiB").validate[String]
                val storageGBJsResult = (sizeBranch \ "storageGB").validate[String]
                val valColumnsJsResult = (sizeBranch \ "valueColumns").validate[JsArray]
                if (sizeJsResult.isSuccess && vCPUJsResult.isSuccess && eCUJsResult.isSuccess &&
                  memoryGiBJsResult.isSuccess && storageGBJsResult.isSuccess &&
                  valColumnsJsResult.isSuccess && valColumnsJsResult.get.value.size == 1) {
                  val valColumns = valColumnsJsResult.get.value(0)
                  val nameJsResult = (valColumns \ "name").validate[String]
                  val priceJsResult = (valColumns \ "prices" \ "USD").validate[String]
                  if (nameJsResult.isSuccess && priceJsResult.isSuccess) {
                    sizeResult :+ new InstanceDetail(region, instanceType, sizeJsResult.get,
                      vCPUJsResult.get, eCUJsResult.get, memoryGiBJsResult.get, storageGBJsResult.get, nameJsResult.get, priceJsResult.get)
                  } else {
                    sizeResult
                  }
                } else {
                  sizeResult
                }
              })

              instanceTypeResult ++ instanceDetails
            } else {
              instanceTypeResult
            }
          })
          regionResult ++ instanceTypesMap
        } else {
          regionResult
        }
      })
      //      regionsMap.map(println(_))
      return regionsMap
    }
    return List[InstanceDetail]()
  }

  def getAllAwsDetails:List[InstanceDetail] = {
    val urls = List(("http://a0.awsstatic.com/pricing/1/ec2/linux-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/mswin-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/rhel-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/sles-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/mswinSQL-od.min.js", "resources/AWS_Details/linux-od.min.js"),
      ("http://a0.awsstatic.com/pricing/1/ec2/mswinSQLWeb-od.min.js", "resources/AWS_Details/linux-od.min.js"))
    return urls.map(DetailGetter.getAwsDetail(_)).flatten
  }

}

object DetailGetterTest extends App {
  DetailGetter.getAllAwsDetails.map(println(_))
}