package forms

import play.api.data.Form
import models.Weights
import play.api.data.Forms.{ mapping, longNumber, nonEmptyText, text }
import play.api.data.Forms.boolean

object WeightsForm {
  val weightsForm: Form[Weights] = Form(
    mapping(
      "w1" -> longNumber,
      "w2" -> longNumber,
      "w3" -> longNumber,
      "w4" -> longNumber,
      "w5" -> boolean)(Weights.apply)(Weights.unapply)
      verifying ("All weights cannot be zeros", ws => {
        ws.w1 + ws.w2 + ws.w3 + ws.w4 != 0
      })
      )
}

