package forms

import play.api.data.Form
import models.Weights
import play.api.data.Forms.{ mapping, longNumber, nonEmptyText, text }
import play.api.data.Forms.boolean
import models.WeightsUpdate

object WeightsUpdateForm {
  val weightsUpdateForm: Form[WeightsUpdate] = Form(
    mapping(
      "w1" -> longNumber,
      "w2" -> longNumber,
      "w3" -> longNumber,
      "w4" -> longNumber,
      "w5" -> boolean,
      "firstSelection" -> text,
      "secondSelection" -> text,
      "thirdSelection" -> text)(WeightsUpdate.apply)(WeightsUpdate.unapply)
      verifying ("All weights cannot be zeros", ws => {
        ws.w1 + ws.w2 + ws.w3 + ws.w4 != 0
      })
      verifying("All selection must be selected", wu => {
        !wu.firstSelection.trim().isEmpty() && !wu.secondSelection.trim().isEmpty() && !wu.thirdSelection.trim().isEmpty()
      })
      )
}